# End2End Simulation

This is a detector simulation for LHCf. Simulations with both LHC and SPS beam test configurations can be performed.

## Configure E2E for your simulation

There are several configurations of E2E simulations; LHC with old LHCf detectors (plastic scintillators), with new LHCf detectors (GSO scintillators), with LHCf + ZDC, SPS Beam Test. For configuring it, you must set the source codes and parameter files correctly.


**== Compile ==**  

you need to set the symbolic link of src/ktower.f and src/interface.f to
* LHC with the old detector (2010 and 2013): ?? and ??
* LHC with the new detector (>=2015):  *ktower.f.gso.lhc* and *interface.f.gso.lhc*
* LHC with the new detector + ZDC (>2022) : *interface.f.lhc2022* and *ktower.f.gso.lhc*
* SPS with new detector : *interface.f.gso.sps* and *ktower.f.gso.sps*
* SPS with Arm1 + ZDC (SPS 2021) : *interface.f.sps2021* and *ktower.f.sps2021*

Additionally, posdepP.h which treats the light yield position dependency of each scintillator layers, readGSOX.f and readGSOAtten.f  which treat the crosstalk and attenuation of GSO bar hodoscopes, must be changed depending on old or new detectors. However, only files for new detectors are available on this repository.  

Then compile as 
```
make clean; make
```
Before compile, you must configure the EPICS to use the "ciecone" shape (see  [install_EPICS.md](install_EPICS.md)).  The compile takes a long time, about 1 hour. It is because all parameters of light yield position dependency is directly given in a header file (posdepP.hOrig). Ohashi tried to modify it to read these parameter from a text file at the execution. However it is not tested yet and the default is the old method. 

**== Parameter files ==**

All necessary parameter files are specified in FirstInput files. Only you need to choose the FirstInput file for your configuration. 

* LHC with New Arm1 Detector :  *FirstInput_arm1.LHC*
* LHC with New Arm2 Detector :  *FirstInput_arm2.LHC*
* LHC with New Arm1 Detector + ZDC : *FirstInput_arm1.LHC2022*
* LHC with New Arm2 Detector + ZDC : *FirstInput_arm2.LHC2022*
* SPS with New Arm1 Detector :  *FirstInput_arm1.SPS*
* SPS with New Arm2 Detector :  *FirstInput_arm2.SPS*
* SPS with New Arm1 Detector + ZDC:  *FirstInput_arm1.SPS2021*

These FirstInput files contain the file path of geometory file (*.cnfg), epicsfile, sepicsfile and param. 

**== epicsfile ==**  
epicsfile sets many parameters related to the electromagnetic interaction. Users should not modify any parameters in the file.
Only thing you need is to change the symbolic link depending to your COSMOS/EPICS versions and your cluster system.  

* Cosmos8.042/Epics9.311 (default since Op2022): epicsfile_epics9
* Cosmos7.645/Epics9.165 (default before Op2022): epicsfile_icrr or epicsfile_lhcfs2 or ...   

Newer Epics 9.311 accept the environmental valuable EPICSTOP in epicsfile, whole old Epics cannot (need to specify the directory path of EPICSTOP directly.). Additionally, the format of several parameters are different between the versions. So please do not mix them up.

**== sepicsfile ==**  
This file specify the parameters relating to incident particles, the number of events etc. Users may need to modify this file for your simulation purpose. Important parameters are,  
*  Ir1(1) and  Ir1(2) : Random seed 
*  Nevent : Number of events. (If you use *+primary*, you should set a number with more than #events in +primary.)
*  PrimaryFile : define primary particle. For SPS, use *primary* to specify only particle and energy. For SPS, use *+primary* to specify incident particle event-by-event. 
*  InputP : Mode specification of incident particle position. 'u+z' means uniform in x and y specified in Xrange / Yrange with injection from positive z-axis direction. it works only in case of *primary*.
*  Xrange / Yrange : Incident position ranges in x and y (cm). 

**== param ==**  
The parameter file "*param*" specifies the hadronic interaction model. The default is 
```
 $PARAM
 IntModel = '"nucrin" 5 "dpmjet3"'
 $END
```
and it should not be changed except specific MC samples.

## How to run 

```
./sepicsPCLinuxIFC64 < FirstInput_arm1.LHC2022 >& output.txt 
```

## Output format 

**Example of Arm1 Op2022**
```
start          1
 TRG       301.01 18  -1  0  -0.6338  -1.5239   0.0000  1656.61360 -0.07511084 -0.18059403  1656.61321
 scin fd    1  1  0.179067E-02
 scin fd    1  2  0.133307E-02
 scin fd    1  3  0.567911E-02
 scin fd    1  4  0.499160E-04
 gsoplate t2    2  1  0.752241E-03  0.753939E-03
 gsoplate t2    2  2  0.161551E-03  0.166421E-03
 gsoplate t2    2  3  0.343909E-03  0.325279E-03
 gsoplate t2    2  4  0.121942E-01  0.123155E-01
 gsoplate t2    2  5  0.386590      0.360332
 gsoplate t2    2  6   1.27137       1.19701
 gsoplate t2    2  7   2.33686       2.19736
 gsoplate t2    2  8   2.41048       2.25900
 gsoplate t2    2  9   2.17996       2.08522
 gsoplate t2    2 10   1.77801       1.67794
 gsoplate t2    2 11   1.20943       1.16384
 gsoplate t2    2 12  0.479650      0.470726
 gsoplate t2    2 13  0.506690      0.493236
 gsoplate t2    2 14  0.700582      0.686217
 gsoplate t2    2 15  0.368450      0.359384
 gsoplate t2    2 16  0.202324      0.202058
 gsobarx t2    2 1  1   0.788072430E-04   0.783202122E-04   0.788072430E-04
 gsobarx t2    2 1  4   0.206548852E-03   0.206576777E-03   0.206548852E-03
 gsobarx t2    2 1 10   0.201844960E-03   0.201987685E-03   0.201844960E-03
 gsobarx t2    2 1 19   0.514196545E-04   0.489919585E-04   0.514196545E-04
 ...
 ion  1   0.0000000
 ion  2   0.0000000
 ion  3   0.0000000
 ion  4   0.0000000
 zdcf  1  1     1.128623527
 zdcf  1  2     1.120563624
 zdcf  1  3     0.870938492
 ...
 zdcl 32     19.43079749
 zdcl 33     11.07096248
 zdcl 34      8.02205046
 zdcl 35      9.41990642
 zdcl 36      7.25814462
 zdcsum  1    242.04226172
 zdcsum  2     83.86849020
 zdcsum  3    199.03672004
 end          1
```

* TRG:  incident position of the particle. It can be more than one.
  * Format: TRG event_tag particle_code subcode charge x y z total_energy px py pz
  * event_tag: event_id + parent_ID + paritcle_ID (see PrimaryFilter)
  * x, y, z : incident position (cm)
  * total_energy, px, py, pz: 4-vector momentum (GeV)
* scin fd : energy deposit on the FC (Gev). The FC shape is different between Arm1_OP2022 and the others.
* gsoplate : energy deposit on the GSO layers 
  * The light yield (L.Y.) efficiency is considered in this E2E.
  * Format: gsoplate tower_name tower_id layer dE_1 dE_2
  * tower_name: t2 / t4 / t25 /t32
  * tower_id: 2 4 25 32
  * layer: (1-16)
  * dE_1:  energy deposit (GeV) with L.Y. efficiency. It should be used for the analysis.
  * dE_2:  energy deposit (GeV) w/o L.Y. efficiency. It is for check
* gsobarx / gsobary: energy deposit on the each GSO bar of the hodoscopes.
  * Attenuation in the fiber is considered in E2E, whole the cross-talk is not. (The cross-talk effect is considered in MCtoLevel0 of LHCfLibrary.)
  * Format : gsobarx tower_name tower_id layer_id bar_id dE_1 dE_2 dE_3
  * layer_id : (1-4)
  * bar_id : (1-20) or (1-40)
  * dE_1:  energy deposit (GeV) (not use = dE_3)
  * dE_2:  energy deposit (GeV) with attenuation effect. It should be used for analysis 
  * dE_2:  true energy deposit (GeV) (w/o attenuation) It is for check. 
* ion : energy deposit in BRAN (not use)
* zdcf, zdcl, zdcsum: effective track length (cm)
  * ZDC sampling layers use quartz fibers (not scintillator) for detection of Cherenkov lights. In this simulation, Cherenkov light production is not considered. Instead, the total track length of charged particles is dumped event-by-event. The Cherenkov light threshold and emission efficiency as a function of beta is considered in this calculation.
  * zdcf is for each fiber. zdcl is summation of each sampling layer. zdcsum is summation of each module. 
# CRMC

## CRMC 

* Installation 
   * CRMC v1.8.0 is available in ICRR cluster. The newest version of CRMC is v2.0.1, however the ICRR default configuration does not fit the requirement of the version. (No mention about gcc version in the CRMC webpage but it could not be compiled with gcc 4.8.5.). The available models of v1.8.0 are same as these of v2.0.1. 
   * For proton-Oxygen collisions, it is better to use CRMC 2.0.1 because HepMC2 (used in v1.8.0) can treat only less than 10000 particles (including intermediate particles) while HepMC3 (used in v2.0.1) does not have such limitation.  
   * Update on Dec. 2024: CRMC 2 was installed (un-official release, hash 34155f79 on https://gitlab.iap.kit.edu/AirShowerPhysics/crmc) to the ICRR cluster and QGSJET III is available on the version.  

* How to use.
   * You need to use the special parameter file prepared for the LHCf simulation, crmc_lhcf.param. 
      * restrict the decay in addition to the default particles; pi0, eta, K0s, K0l, lambda + particles with ctau > 1cm
      * the particle ID of EPOS can be found in (CRMC source)/src/epos/epos-ids.f. For cosmos/epics, the ID definition can be found in (Cosmos source)/cosmos/Zcode.h.
      * crmc_lhcf.param for ICRR is available in /CRMC/src/crmc. In other systems (or for other CRMC version), you need to modify the pass of CRMC data files in the file to fit your environment.
   * For mass production, 
      * the output format must be gzipped HepMC. (it is required for the conversion code)
      * The random seed of CRMC should be given as Run Number. (-s option of crmc like -s ${Run})
   * A script for the mass production can be found in [CRMC/job_control_icrr/crmc/run.sh](CRMC/job_control_icrr/crmc/run.sh).
   * Example for an event generation with EPOS-LHC
   ```
   crmc-v1.8.0/bin/crmc -o root -i2212 -I2212 -p6800 -P-6800 -n10 -s 1 -c crmc_lhcf_icrr.param -f EPOS.root -m0
   ```
   * crmc options 
   ```
   Options of CRMC:
  -h [ --help ]                      description of options
  -v [ --version ]                   show version and exits
  -o [ --output ] arg                output mode: hepmc (default), hepmcgz,
                                     root, lhe, lhegz
  -s [ --seed ] arg                  random seed between 0 and 1e9 (default:
                                     random)
  -n [ --number ] arg                number of collisions
  -m [ --model ] arg                 model [0=EPOS_LHC, 1=EPOS_1.99,
                                     2=QGSJET01, 4=Pythia_6.4.28,
                                     6=Sibyll_2.3d, 7=QGSJETII-04,
                                     11=QGSJETII-03, 12=DPMJet-III_2019.1]
  -p [ --projectile-momentum ] arg   momentum/(GeV/c)
  -P [ --target-momentum ] arg       momentum/(GeV/c)
  -S [ --sqrts ] arg                 sqrt(s/GeV**2)
  -i [ --projectile-id ] arg         PDG or Z*10000+A*10
  -I [ --target-id ] arg             PDG or Z*10000+A*10
  -c [ --config ] arg                config file
  -f [ --out ] arg                   output file name (auto if none provided)
  -t [ --produce-tables ] [=arg(=1)] create tables if none are found
  -T [ --test ] [=arg(=1)]           test mode
  -x [ --cross-section ] [=arg(=1)]  calculate and print cross section only
  ```

## Conversion from HepMC format to Gencol format (input of DoubleArm)

* General information 
   * The code converts the format of CRMC output files from HepMC to Gencol. The output files can be input files of DoubleArm simulation. 
   * The following event selection is also applied in the code:  
       * cos(theta) > 0.9999995 (corresponding 13 cm / 140 m)
   * This applies the crossing angle.
   * The last number of each line is for event ID number with the format of XXXXXXYY; XXXXXX=#collisions, YY=#particles in the collision after the event selection.
    
* How to use 
   * compile the code by 'make' in lhcf-simulation/CRMC/src/conversion/
   * example ``` ./bin/HepMCtoGencol -i crmc.out.gz -o gen.out -c 145 -b ```
   * options
   ```
   -i                      filename
   -o                      output file path (use this if you want to have only one output file)
   -O                      output directory
   -F                      Format of output filename (default= gen.%%06d.out)
   -c                      beam crossing angle [urad] (default = 145)
   -b                      apply crossing angle by boost instead of simple rotation
   -p                      remove pT=0 GeV/c events
   -d                      debug (provide debug filename like '-d debug001.root')
   -h                      help
   ```

* Note
   * The beam parameters (beam energy, mass of proton) are given in inc/constants.h 

## Mass production for Run3 (pp, 13.6 TeV)

* General information
   * used CRMC v1.8.0 (gcc 4.8.5 in ICRR cluster)
   * 10^5 collision per run (a file)
   * filename format of CRMC output: crmc_{model name}_{run number:%06d}.hepmc.gz e.g. crmc_EPOSLHC_000001.hepmc.gz
   * filename format of converted file: gen_{model name}_{run number:%06d}.out.gz e.g. gen_EPOSLHC_000100.out.gz
   * conversion option: -c 145 -b  (if QGSJET II-04, additionally -p)
   * file size for 1 run (10^5 collisions)
      * CRMC : 1 GB
      * Conv.: 25 MB  

* in ICRR Cluster 
   * Directory for job scripts: /disk/lhcf/user/simulation_Run3/lhcf-simulation/CRMC/job_control_icrr/crmc
   * Directory for data: /disk/lhcf/user/simulation_Run3/Data/crmc_1.8.0/ 

* Note
   * One file contains for 10^5 collisions, which is limited from the calculation time of < 1 days. 
       * EPOSLHC: ~ 3 hours for 10^5 collisions
       * QGSJET II-04: ?? hours for 10^5 collisions 

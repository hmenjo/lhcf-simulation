# Installation of COSMOS and EPICS

Both COSMOS and EPICS require Intel Fortran Compiler. The newest version of COSMOS and EPICS can be compiled by GCC but the LHCf DoubleArm and End2End cannot be done.

## Install COSMOS

1. Download the source code and unzip it  
```
wget https://cosmos.n.kanagawa-u.ac.jp/cosmosHome/Cosmos/Cosmos8.042.tar.gz
tar zxvf Cosmos8.042.tar.gz
```  

2. Setup the configuration and compile the source code  
```
cd Cosmos8.042
cp config/site.configPCLinuxIFC64 ./site.config  # configuration for Intel Compiler
source Scrpt/sevi.sh
make 
```
The command ```source Scripts/sevi.sh``` sets the environmental valuables (COSMOSTOP and PATH). You can set them manually also as 
```
export COSMOSTOP=/path/to/cosmos
export COSMOSINC=${COSMOSTOP}/cosmos/
export PATH=$COSMOSTOP/Scrpt/:$PATH
```

3. ~~Download the parameter file for QGSJETII-04.~~  
```
cd Import/Hidden/QGS/qgsjetII-04
wget https://cosmos.n.kanagawa-u.ac.jp/cosmosHome/Cosmos/qgsdat-II-04.gz
```
Cosmos8.042 includes the parameter file in the source code. For other versions, you may need to do this step. 

## Install EPICS

1. Download the source code and unzip it
```
wget https://cosmos.n.kanagawa-u.ac.jp/EPICSHome/Epics9.311.tar.gz
tar zxvf Epics9.311.tar.gz
```
2. Setup the configuration and compile the source code  
```
cd Epics9.311/
cp site.configPCLinuxIFC64 site.config
source Scrpt/sevi.sh
make 
```
The command 'source Scrpt/sevi.sh' can be replaced as similar as COSMOS, 
```
export EPICSTOP=/path/to/epics
export EPICSINC=$EPICSTOP/epics
export PATH=$EPICSTOP/Scrpt/:$PATH
```

3. Setup for End2End simulation  
DoubleArm and End2End uses a non-default geometrical shape. To activate it, the following step should be done.
```
cd Epics9.311/Util/UseNewVol/
./usenewvol /(your path)/lhcf-simulation/End2End/src/cnfg/dummy_newval.cnfg
```

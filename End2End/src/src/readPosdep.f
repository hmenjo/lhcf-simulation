      real(8):: posdepP20(16,21,21)
      real(8):: posdepP40(16,41,41)
      real(8):: posdepP25(16,27,27)
      real(8):: posdepP32(16,34,34)

      integer:: ii, jj, kk
      integer,parameter:: io=10
      character(9):: ID
      real(8):: v
      logical,save::  firsttime = .true.
      real(8),parameter:: big=1.d100


      if( firsttime ) then
!!!  #ifdef TESTTIME; next needed only at TESTTIME but
!!!  may be kept; time loss  is negligile
         posdepP20(:, :, :) = big
         posdepP25(:, :, :) = big
         posdepP40(:, :, :) = big
         posdepP32(:, :, :) = big
!!!  #endif

         open(io, file="src/ForPosdep.dat")
!         open(io, file="ForPosdep.dat")

         do 
            read(io, *,end=100) ID, ii, jj, kk, v
     
            select case( trim(ID) ) 
              case( "posdepP20" )
                 posdepP20(ii, jj, kk) = v
              case( "posdepP25" )
                 posdepP25(ii, jj, kk) = v
              case( "posdepP40" )
                 posdepP40(ii, jj, kk) = v
              case( "posdepP32" )          
                 posdepP32(ii, jj, kk) = v
              case default
                 write(0,*) ID, ' invalid in readPosdep'
                 stop
              end select
           enddo
 100       continue
           close(io)
!!! #ifdef TESTTIME
           if( any( posdepP20 == big ) ) then
              write(0, *) 'some posdepP20 not filled '
           endif
           if( any( posdepP25 == big ) ) then
              write(0, *) 'some posdepP25 not filled '
           endif
           if( any( posdepP40 == big ) ) then
              write(0, *) 'some posdepP40 not filled '
           endif
           if( any( posdepP32 == big ) ) then
              write(0, *) 'some posdepP32 not filled '
           endif
!!! #endif

           firsttime = .false.
        endif

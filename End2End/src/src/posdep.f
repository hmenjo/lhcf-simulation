c     modified by Alessio (2014) 
c     - changed interpolation of efficiency for Arm2

      subroutine  plate_eff(nt,nscinl,x,y,eff)
      implicit none
      integer nt,nscinl
      real*8  x,y,xx,yy,xr,yr   ! input
      real*8  eff               ! output
      integer dx,dy
      real*8  x1, x2, y1, y2
      real*8  c00,c10,c01,c11
      real*8  sqrt2
      include 'src/posdepP.h'

      sqrt2=sqrt(2.)

      if(nscinl .eq. 0) then 
         eff = 1.0
         return
      else if(nscinl .le. 16) then
         xx=x*10
         yy=y*10

         if((nt .eq. 1) .or. (nt .eq. 2)) then
c           --- Arm 1 ---
            dx=aint(xx)
            dy=aint(yy)
            if(nt .eq. 1) then
               c00=posdepP20(nscinl,dx+1,dy+1)
               c10=posdepP20(nscinl,dx+2,dy+1)
               c01=posdepP20(nscinl,dx+1,dy+2)
               c11=posdepP20(nscinl,dx+2,dy+2)
            elseif(nt .eq. 2) then
               c00=posdepP40(nscinl,dx+1,dy+1)
               c10=posdepP40(nscinl,dx+2,dy+1)
               c01=posdepP40(nscinl,dx+1,dy+2)
               c11=posdepP40(nscinl,dx+2,dy+2)
            endif

            eff=(dy+1-yy)*(dx+1-xx)*c00
     *           + (dy+1-yy)*(xx-dx)*c10
     *           + (yy-dy)*(dx+1-xx)*c01
     *           + (yy-dy)*(xx-dx)*c11  
            
         elseif ((nt .eq. 3) .or. (nt .eq. 4)) then
c           --- Arm 2 ---
            if(nt .eq. 3) then
c           --- Small tower ---
               xx = 25. - xx
               if((nscinl .eq. 3) .or. (nscinl .eq. 6)) then
c                 layers 3 and 6 have different positions of measurements
                  xr=(xx-yy+26.5)/sqrt2
                  yr=(xx+yy+0.5)/sqrt2
                  dx=aint((xx-yy+26.5)/2.)
                  dy=aint((xx+yy+0.5)/2.)
               else
                  xr=(xx-yy+25.)/sqrt2
                  yr=(xx+yy)/sqrt2
                  dx=aint((xx-yy+25.)/2.)
                  dy=aint((xx+yy)/2.)
               endif
               c00=posdepP25(nscinl,dx+1,dy+1)
               c10=posdepP25(nscinl,dx+2,dy+1)
               c01=posdepP25(nscinl,dx+1,dy+2)
               c11=posdepP25(nscinl,dx+2,dy+2)
            elseif(nt .eq. 4) then
c           --- Large tower ---
               xx = 32. - xx
               if((nscinl .eq. 3) .or. (nscinl .eq. 6)) then
c                 layers 3 and 6 have different positions of measurements
                  xr=(xx-yy+33.5)/sqrt2
                  yr=(xx+yy+1.5)/sqrt2
                  dx=aint((xx-yy+33.5)/2.)
                  dy=aint((xx+yy+1.5)/2.)
               else
                  xr=(xx-yy+33.5)/sqrt2
                  yr=(xx+yy+0.5)/sqrt2
                  dx=aint((xx-yy+33.5)/2.)
                  dy=aint((xx+yy+0.5)/2.)
               endif
               c00=posdepP32(nscinl,dx+1,dy+1)
               c10=posdepP32(nscinl,dx+2,dy+1)
               c01=posdepP32(nscinl,dx+1,dy+2)
               c11=posdepP32(nscinl,dx+2,dy+2)
            else
               eff = 1.0
               return
            endif
            
            if( (c00 .lt. 0) .or. (c10 .lt. 0)
     *           .or. (c01 .lt. 0) .or. (c11 .lt. 0) ) then
               eff = 0.0
               return
            endif
            
            x1=dx*sqrt2
            x2=(dx+1)*sqrt2
            y1=dy*sqrt2
            y2=(dy+1)*sqrt2
            
            eff=((y2-yr)*(x2-xr)*c00
     *           + (y2-yr)*(xr-x1)*c10
     *           + (yr-y1)*(x2-xr)*c01
     *           + (yr-y1)*(xr-x1)*c11)/2.

         else
            eff = 1.
            return
         endif
         
         return
      else 
         eff = 0.0
         return
      endif
      end

      subroutine bar_eff(tower, layer, xory, x, y, eff)
      implicit none
      character(2) tower
      integer layer
      character xory 
      real*8 x,y,eff
      integer x0, x1, bhit
      real*8 y0, y1, xx, yy, aa
      real*8 lbar
      include 'src/GSObarAttenuation.h'

c     xx=y*10
c     yy=x*10
      xx=x*10
      yy=y*10

      if(tower .eq. 't2') then
         lbar=20.
         if(xory .eq. 'x') then
            bhit=aint(xx)+1
            aa=lbar-yy
            x0=aint(aa)
            x1=x0+1
            y0=attX20(layer, bhit, x0+1)
            y1=attX20(layer, bhit, x1+1)
         elseif(xory .eq. 'y') then
            bhit=aint(yy)+1
            aa=lbar-xx
            x0=aint(aa)
            x1=x0+1
            y0=attY20(layer, bhit, x0+1)
            y1=attY20(layer, bhit, x1+1)
         endif
      elseif(tower .eq. 't4') then
         lbar=40.
         if(xory .eq. 'x') then
            bhit=aint(xx)+1
            aa=lbar-yy
            x0=aint(aa)
            x1=x0+1
            y0=attX40(layer, bhit, x0+1)
            y1=attX40(layer, bhit, x1+1)
         elseif(xory .eq. 'y') then
            bhit=aint(yy)+1
            aa=lbar-xx
            x0=aint(aa)
            x1=x0+1
            y0=attY40(layer, bhit, x0+1)
            y1=attY40(layer, bhit, x1+1)
         endif
      endif

      eff=(y1-y0)/(x1-x0)*(aa-x0)+y0

c$$$  if(layer .eq. 1) then
c$$$  write(*,'(a,2f15.10)') 'hit bar: ', x*10, y*10
c$$$  write(*,'(a,a,i3,a,f15.10,2i3,i3,3f15.10)')
c$$$  *          '    ',tower,layer,xory, aa,x0,x1, bhit,y0,y1,eff
c$$$  endif

      end

!!! #define TESTTIME

      integer,parameter::is=4, js=60, ks=64

      real(8):: xtalkX(is, js, ks)
      real(8):: xtalkY(is, js, ks)
      real(8):: xtalkX_r(is, js, ks)
      real(8):: xtalkY_r(is, js, ks)

      integer:: ii, jj, kk
      integer,parameter:: io=10
      character(8):: ID
      real(8):: v
      logical,save::  firsttime = .true.
      real(8),parameter:: big=1.d100


      if( firsttime ) then
!!!  #ifdef TESTTIME; next needed only at TESTTIME but
!!!  may be kept; time loss  is negligile
         xtalkX(:, :, :) = big
         xtalkY(:, :, :) = big
         xtalkX_r(:, :, :) = big
         xtalkY_r(:, :, :) = big
!!!  #endif

         open(io, file="src/ForGSO.dat")

         do 
            read(io, *,end=100) ID, ii, jj, kk, v
     
            select case( trim(ID) ) 
              case( "xtalkX" )
                 xtalkX(ii, jj, kk) = v
              case( "xtalkY" )
                 xtalkY(ii, jj, kk) = v
              case( "xtalkX_r" )
                 xtalkX_r(ii, jj, kk) = v
              case( "xtalkY_r" )          
                 xtalkY_r(ii, jj, kk) = v
              case default
                 write(0,*) ID, ' invalid'
                 stop
              end select
           enddo
 100       continue
           close(io)
!!! #ifdef TESTTIME
           if( any( xtalkX == big ) ) then
              write(0, *) 'some xtalkX not filled '
           endif
           if( any( xtalkY == big ) ) then
              write(0, *) 'some xtalkY not filled '
           endif
           if( any( xtalkX_r == big ) ) then
              write(0, *) 'some xtalkX_r not filled '
           endif
           if( any( xtalkY_r == big ) ) then
              write(0, *) 'some xtalkY_r not filled '
           endif
!!! #endif
!!!   end program
           firsttime = .false.
        endif

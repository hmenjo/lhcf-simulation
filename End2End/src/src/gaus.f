c       gaussian
        subroutine gaussian(mean, sigma, value)
        implicit none

        real*8 rndm
        real*8 mean
        real*8 sigma
        real*8 value

        real*8 kC1
        real*8 kC2
        real*8 kC3
        real*8 kD1
        real*8 kD2
        real*8 kD3
        real*8 kHm
        real*8 kZm
        real*8 kHp
        real*8 kZp
        real*8 kPhln
        real*8 kHm1
        real*8 kHp1
        real*8 kHzm
        real*8 kHzmp

        real*8 kAs
        real*8 kBs
        real*8 kCs
        real*8 kB
        real*8 kX0
        real*8 kYm
        real*8 kS
        real*8 kT

        real*8 result
        real*8 rn,x,y,z

        kC1 = 1.448242853
        kC2 = 3.307147487
        kC3 = 1.46754004
        kD1 = 1.036467755
        kD2 = 5.295844968
        kD3 = 3.631288474
        kHm = 0.483941449
        kZm = 0.107981933
        kHp = 4.132731354
        kZp = 18.52161694
        kPhln = 0.4515827053
        kHm1 = 0.516058551
        kHp1 = 3.132731354
        kHzm = 0.375959516
        kHzmp = 0.591923442
c        zhm 0.967882898

        kAs = 0.8853395638
        kBs = 0.2452635696
        kCs = 0.2770276848
        kB  = 0.5029324303
        kX0 = 0.4571828819
        kYm = 0.187308492 
        kS  = 0.7270572718 
        kT  = 0.03895759111

        call random_number(y)

        if(y .gt. kHm1) then
           result = kHp*y-kHp1
           goto 1234
        elseif(y .lt. kZm) then  
           rn = kZp*y-1
           if(rn .gt. 0) then
              result = 1+rn
              goto 1234
           else
              result = -1+rn
              goto 1234
           endif
        elseif(y .lt. kHm) then  
           call random_number(rndm)
           rn = rndm
           rn = rn-1+rn
           if(rn .gt. 0)  then
              z = 2-rn
           else
              z = -2-rn
           endif
           if((kC1-y)*(kC3+abs(z)) .lt. kC2) then
              result = z
              goto 1234
           else
              x = rn*rn
              if((y+kD1)*(kD3+x) .lt. kD2) then
                 result = rn
                 goto 1234
              elseif(kHzmp-y .lt. exp(-(z*z+kPhln)/2)) then
                 result = z
                 goto 1234
              elseif(y+kHzm .lt. exp(-(x+kPhln)/2)) then
                 result = rn
                 goto 1234
              endif
           endif
        endif

        do
           call random_number(rndm)
           x = rndm
           call random_number(rndm)
           y = kYm * rndm
           z = kX0 - kS*x - y
           if(z .gt. 0) then
              rn = 2+y/x
           else
              x = 1-x
              y = kYm-y
              rn = -(2+y/x)
           endif
           if((y-kAs+x)*(kCs+x)+kBs .lt. 0) then
              result = rn
              goto 1234
           elseif(y .lt. x+kT) then
              if(rn*rn .lt. 4*(kB-log(x))) then
                 result = rn
                 goto 1234
              endif
           endif

        enddo

           
 1234   value = mean + sigma * result

c        write(*,*) 'mean: ', mean, ' sigma: ', sigma, ' value: ', value
        end

Model parameters for sepics.
evAny comment may be placed before ---- line.
you must give a value for each row at a column corresponding to
  "specified"   if you give / only there, the standard is taken.
  No items  should be deleted or added.  If the column for the
  standard has only ",", no  standard value is defined.

The name field must start from the 2nd column.
Blank lines, if any,  are ignored. If the first 2 columns are blank,
the line is regarded as a comment line and ignored.



name    standard   specified    meaning    options & note
----------------------------------------------------------------------
 BaseTime     1.             / average cpu time(sec) for 1 GeV.

 CosNormal  (1.0,  1.)        / The range of the cosine of the angle 
	        between the "normal vector" and the incident particle
       	     	direction.  Used only if InputA = 'is' or 'cos *'
	        The normal vector is directed to +z axis normally
	        but actually it depends on InputP as follows.

                InputP = 'fix':
                     +z axis   
	        InputP(2:3) = one of '+z', '-z', '+x', '-x', '+y', '-y'
                      Here "(2:3)"  means 2nd to 3rd characters of InputP.
	              This shows the normal vector direction. For example,
	              if it is '+x', the normal vector is directed to
	              + x axis direction so that CosNormal = (1.,1.)
	              means that the beam is directed to +x axis exactly.
	        InputP(2:4)  = 'sph'
                     In this case, incident point is distributed 
                     on the world sphere surface.  The normal vector is the
	             vector directed from the incident position to the
                     center.  If the incident  particle is  found not 
	             directed to the inner part of the sphere,  the particle
                     direction is reversed.
                
                      
 DCInpX     0.d1           /  These three are x, y, z components of the 
 DCInpY     0.d1           /  direction cosine of  the incident ptcl.
 DCInpZ     1.d0,          /  Used only if InputA is 'fix'.
                              The square sum must be equal to 1. within 
                      the accuracy of 4~5 digits. Since it is bothersome
                      to give higher accuracy, sepics renormalize them.

                      The value is for the world  coordinate.

 DeadLine  '00.00.00'        /     Deadline time  (not used yet)
 E0Max     300000.         / If the sampled incident energy > E0Max, it will
                           be discarded.
 Hwhm       ,         /   Half width at half max of a Gaussian spread beam.
                    Used when InputP(1:1)='g' (cm) (see also ProfR)
 InputA    'fix'         / 'is' --> Incident particle angle is isotropic
                                   (See CosNormal).
                          'fix'--> Incident particle angle is a fixed one
                                   given by the direction cosine,
                                   (DCInpX, DCInpY, DCInpZ).
                          'cos pw'--> cos^pw dcos angular distribution
                           if pw = 0 equivalent to 'is'.  
 InputP    'u+z'      /  Specify the incident partilce position in the
                          world coordinate.  
        InputP= 'fix'  (Xinp, Yinp, Zinp) (cm) is used.

        InputP= 'usph'  uniform on the world sphere.
                  Therefore, the world must be given by a sphere.
                  The region of the input position  can be controled by 
                  Xinp, Yinp and Zinp. 
                  *******************
                  Xinp and Yinp are regarded as a polar angle (theta deg.)
                  and azimuthal angle (phi deg.) around which the incident
                  particles are distributed within an half opening angle of
                  Zinp (deg). 
        InputP='gsph'  Gaussian beam on the world sphere. Xinp, Yinp and Zinp
                  serve as in the case of 'usph'.  ProfR is not referred
	          since Zinp can do the same. Gaussian means here that
                  the beam has a Gaussian density if it is  projected to
                  the plane which is tangent to the sphere at the  Gaussian 
                  center. See also CosNormal.

        InputP='u-x', 'u+z' etc 
	          uniform within a box given by Xrange, Yrange, Zrange.
	          If you want to start the beam from z=0 with uniform
                  (x,y) within Xrange, Yrange, you may set, e.g,
                  InputP='u+z' and Zrange=(0.,0.), Xrange=(0.,10.),
                  Yrange=(-5.,5.).
        InputP='g-x', 'g+z' etc.
                  Gassian beam: The Gasussian beam spread is
                  controled by Hwhm (Half-width at half-max) and ProfR.
                  The latter is the radius of the beam beyond which no
                  particle is distributed.
	          The beam center is (Xinp, Yinp, Zinp). The beam
	          will be distributed on a plane perpendicular to the
	          beam direction specified by +x, -z etc. For example
                  If you put a Gaussian beam on (x,y) plane at z=0,
                  InputP='g+z' Zinp=0.
      
            If these input positions do not satisfy you, you have to make
            the incident particle in your userhook rouitne.
 
 Ir1(1)   1099918     / Random number seed: The generator is the same as
 Ir1(2)   -127993    / the one used in Cosmos. It can be initialized
                             by two 32-bit integers.  
 JobTime   1190           /  cpu time alloted (not used yet)
 Nevent        100     /   # of events to be generated.
 PrimaryFile  'primary.photon100'   / primary spectrum data file.
                            For details, see $EPiCSTOP/Data/Primary/sample.d.
                            The format is completely the same as Cosmos.
 ProfR      ,            /   Limit of gaussian beam radius (cm).
                             Used when InputP is 'gxy' etc.
 Xinp     -0.45    -0.45    /   Incident pos of x. Used when InputP='fix'

 Yinp     0    0   /   see Xinp. (see InputP in document)

 Zinp      0.      /   see Xinp
                           The values are in cm in the world coordinate.
                           These are used quite a different purpose if
                           InputP='usph'  or 'gsph'. See InputP.

 LogIr     t           / Take log of intial seed of random num. etc 
                         for each event. Output will appear on the
                        error out.
 Xrange (-4.27,-0.89)     /  These 3 are used when InputP='u-x', 'u+z' etc.
 Yrange (-0.66, 2.72)     /  Input particle position is uniformly sampled 
 Zrange (0, 0)            /  within the box specifed by these 3. 
                          world coordinate in cm.
                          25mm : X= (-1.80, 0.90), Y= (-1.35, 1.35) OLD
                          32mm : X= ( 0.90, 4.30), Y= ( 1.35, 4.75) OLD
                          25mm : X= (-0.89, 1.79), Y= (-1.34, 1.34) NEW
                          32mm : X= (-4.27,-0.89), Y= ( 1.34, 4.72) NEW

 epHooks '7  4  0'       / Entry for giving user defined parametes.
                         Parameters can be a maximum of 5 character
	                 strings(max of 100 characters each),
                         10 integers and 10  real*8 numbers.
                         This entry specified how many such parameters
                         follow this. If '0 0 0', no user defined
                         parameters are assumed.
                         The defalut limit "5, 10, 10" can be enlarged
                         by changing epics/ZepMaxdef.h
 epHookc '1 8   8    4 0 0 fd  0'   / tw# xsize ysize scinlayers scifilayers outxymap name scinpd
 epHookc '2 2.5 2.5 16 0 0 t25 1'   / tw# xsize ysize scinlayers scifilayers outxymap name scinpd
 epHookc '3 3.2 3.2 16 0 0 t32 1'   /
 epHookc '4 4   4    1 0 0 i1  0'   /
 epHookc '5 4   4    1 0 0 i2  0'   /
 epHookc '6 4   4    1 0 0 i3  0'   /
 epHookc '7 4   4    1 0 0 i4  0'   /
 epHooki  0              /   outxymap or not (0/1)
 epHooki  7              /   # of towers
 epHooki  0	        /   scifi, map pos in world coordinate. (0-->integer local coord)
 epHooki  8              /   # of silicon layers
                      Example:
                   epHooks '2 1 2'  /
                   epHookc 'checkfile'  /
                   epHookc '1  3  -9.0'   /
                   epHooki  -1            /
                   epHookr 100.          /
                   epHookr -2.5       /
                     This example shows that 2 strings, 1 integer and 2 real*8
                     numbers follow this in this order.
                     They can be accessed in your program by
                         call epqHookc(i, cv)  ! to inquire i-th string. 
                                               !    cv will recieve the string.
                         call epqHooki(i, iv)  ! to inquire i-th integer value
                                               !    iv will get the value 
                         call epqHookr(i, rv)  ! to inquire i-th real*8 value
                                               !    rv will get the value.
                         If i is out of range, cv=' '
                                               iv=-9999999
                                               rv=-1.d-60
                   These inquiry subroutines may be called whenever/whereever
                   you need (after sepics paramers is read). 
        **Note**   There is the same entry in epicsfile, but if this is give
	           here, the entry in epicsfile is ignored.

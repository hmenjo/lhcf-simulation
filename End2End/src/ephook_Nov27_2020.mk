#include  $(EPICSTOP)/site.config
include  ./site.config

FFLAGS += -S
INCHEADER  +=  -I./src
INCHEADER  +=  -I$(EPICSTOP)/UserHook/
#objs =  ephook.o   eppos2B.o
objs =   ephook.o  eppos2B.o  mymain.o

sepics$(ARCH): $(objs)
	$(LD) $(LDFLAGS) -o $@ $(objs) -v  -L$(DEST) -l$(LIBNAME) -L$(COSMOSTOP)/lib/$(ARCH) -lcosmos

clean:;		@rm -f $(OBJS) core *~ a.out

ephook.o: $(EPICSINC)/ZepTrack.h \
	$(EPICSINC)/Zmove.h \
	$(EPICSINC)/Zepcondc.h \
	$(EPICSINC)/BlockData/epblkepi.h \
	$(EPICSINC)/BlockData/epblksepi.h \
	$(EPICSINC)/BlockData/epblkCnfig.h 



#ephook.o: $(EPICSINC)/Zepdef.h \
#	$(COSMOSINC)/BlockData/cblkTracking.h 

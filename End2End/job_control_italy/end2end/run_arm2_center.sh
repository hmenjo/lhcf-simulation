#!/bin/bash

LHCF_SIM_BASE="/opt/exp_software/lhcf/Simulation/LHCfSimulation/"
source ${LHCF_SIM_BASE}/End2End/setup_epics_cnaf.sh

if [ $# -ne 6 ]; then
    echo "Usage: $0 <GENERATOR> <INPUT_TAG> <INPUT_FILE> <OUTPUT_DIR> <OUTPUT_FILE> <SEED>"
    exit 1;
fi 

GENERATOR=$1
INPUT_TAG=$2
INPUT_FILE=$3
OUTPUT_DIR=$4
OUTPUT_FILE=$5
SEED_NUMBER=$6

MODEL=${GENERATOR}

WORKDIR="${OUTPUT_DIR}/work/${INPUT_TAG}"
INPUT_FILENAME=${INPUT_FILE}
OUTPUT_FILENAME=${OUTPUT_FILE}
INPUT_COMPLETED="${OUTPUT_DIR}/done/"

# Setup 
mkdir -p ${WORKDIR}
mkdir -p ${OUTPUT_DIR}/old
mkdir -p ${INPUT_COMPLETED}
cd ${WORKDIR}

date

# make symbolic link for input file 
if [ -f "$INPUT_FILENAME" ];then
    ln -s ${INPUT_FILENAME} +primary
else 
    echo "ERROR: No input file was found"
    exit 
fi

# Check presence of old file
if [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME.gz" ];then
    command="mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old"
    echo "$command"
    $command
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
elif [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME" ];then
    command="mv $OUTPUT_DIR/$OUTPUT_FILENAME $OUTOUT_DIR/old"
    echo "$command"
    $command
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
fi

END2END_DIR="${LHCF_SIM_BASE}/End2End/src/" 
END2END_BIN="sepicsPCLinuxIFC64"
# copy files to the working directory
cp $END2END_DIR/$END2END_BIN ./
cp $END2END_DIR/FirstInput_arm2.LHC2022 ./
cp $END2END_DIR/param ./
cp $END2END_DIR/sepicsfile_arm2.gso.lhc ./tmp_sepicsfile
cp $END2END_DIR/epicsfile* ./
cp -r $END2END_DIR/cnfg ./
cp -r $END2END_DIR/src ./

# replace the random seed 
sed -e "s/-123456/${SEED_NUMBER}/g" tmp_sepicsfile  > sepicsfile_arm2.gso.lhc

./$END2END_BIN < FirstInput_arm2.LHC2022 > $OUTPUT_DIR/$OUTPUT_FILENAME 
RETVAL=$?

if [ $RETVAL -eq 0 ]; then 
    echo "Simulation correctly completed"
    grep -cw "total" $OUTPUT_DIR/$OUTPUT_FILENAME
    GRPVAL=$?
    if [ $GRPVAL -ne 1 ]; then
	    echo "File is intact"
	    # gzip the simulated output file
	    gzip -f $OUTPUT_DIR/$OUTPUT_FILENAME
	    # move input file to done folder
	    mv $INPUT_FILENAME $INPUT_COMPLETED
	    echo "Compression end"
    else
	    echo "File is correpted"
	    echo "Avoid compression"
    fi
else 
    echo "Simulation exit with error"
fi 

# remove the directory 
rm -rf $WORKDIR

date


'''
Submit controller of End2End jobs to CNAF cluster.
This works with both python 2.7 and python 3
Please use '_' as a word separation. 

The random seed is automatically set using numbers in the input filename.
'''

import subprocess
import time
import sys
import os
import re
import glob
from datetime import datetime, timedelta
from logging import getLogger, StreamHandler, Formatter, FileHandler, DEBUG
from socket import gethostname

if sys.version_info.major == 3:
	# Python 3	
	unicode = str

class JobController:
	def __init__(self) -> None:
		self.user = os.getenv("USER")
		self.logger = getLogger(__name__)
		
		#HTCondor v9
		#self.hostname = "sn01-htc.cr.cnaf.infn.it"
		#self.jobrm = 'condor_rm -name ' + self.hostname        
		#self.jobsub = 'condor_submit -name ' + self.hostname + ' -spool'
		#self.jobstat = 'condor_q -name ' + self.hostname + ' --nobatch'
		#self.jobtrans = 'condor_transfer_data -name ' + self.hostname
		
		#HTCondor v23
		self.hostname = "sn01-htc -pool cm01-htc"
		self.jobrm = 'condor_rm -name ' + self.hostname        
		self.jobsub = 'condor_submit -name ' + self.hostname + ' -spool'
		self.jobstat = 'condor_q -name ' + self.hostname + ' --nobatch'
		self.jobtrans = 'condor_transfer_data -name ' + self.hostname

		self.htcerror = 'Failed to fetch' 

		self.model = None #Name of the model generator
		self.minrun = None #Number of the first run to include
		self.maxrun = None #Number of the last run to include
		self.indir = None #Folder from where filtered file are
		self.outdir = None #Folder from where filtered file are
	
		self.subname = "run_arm2_center.sub"
		self.shname = "run_arm2_center.sh"

		self.subfile = os.path.dirname(__file__)+'/'+self.subname        
		self.shfile = os.path.dirname(__file__)+'/'+self.shname        
		self.nmaxjobs = 1000
		self.waittime = 1

	def set_user(self, username):
		self.user = username
		
	def set_model(self, generator):
		self.model = generator
		
	def set_minrun(self, minrun):
		self.minrun = minrun

	def set_maxrun(self, maxrun):
		self.maxrun = maxrun

	def set_input(self, inputdir):
		self.indir = inputdir

	def set_output(self, outputdir):
		self.outdir = outputdir
		
	def set_nmaxjobs(self, maxjob):
		self.nmaxjobs = maxjob
			
	def set_waittime(self, waittime):
		self.waittime = waittime
	
	def make_directories(self):
		if not os.path.exists(self.workdir):
		    os.makedirs(self.workdir)
		if not os.path.exists(self.logdir):
		    os.makedirs(self.logdir)
		    os.makedirs(self.logdir+"/htc/")
		    os.makedirs(self.logdir+"/out/")
		    os.makedirs(self.logdir+"/err/")
		    os.makedirs(self.logdir+"/logger/")
		
	def setup_logger(self):
		logfile=self.logdir+'/logger/logger.txt'
		self.logger = getLogger(__name__)
		self.logger.setLevel(DEBUG)
		
		sh = StreamHandler()
		sh.setLevel(DEBUG)
		formatter = Formatter('%(asctime)s: %(message)s')
		sh.setFormatter(formatter)
		self.logger.addHandler(sh)
		
		fh = FileHandler(logfile)  # fh = file handler
		fh.setLevel(DEBUG)
		fh_formatter = Formatter('%(asctime)s : %(message)s')
		fh.setFormatter(fh_formatter)
		self.logger.addHandler(fh)
		return self.logger
			
	def empty(self, pfile):
		out = None
		err = None
		print(pfile)
		command  = 'cat {} | wc -l'.format(pfile)
		while True:
			subpipe  = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)      
			out, err = subpipe.communicate()
			if err != None:
				print("Error while checking file emptyness!")
				return False
			else:
				if "No such file or directory" in str(out):
					print("The file does not exist!")
					return True				
				nline = int(out)
				if nline>2:
					return False
				else:
					return True

	def njob(self):
		out = None
		err = None
		command  = '{} | grep {} | grep {}'.format(self.jobstat, self.user, self.shname)
		while True:
			subpipe  = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)      
			out, err = subpipe.communicate()
			if err != None:
				print("Error while checking the total number of jobs: Retry!")
				time.sleep(60)
			else:
				if self.htcerror in str(out):
					print("Error while checking the total number of jobs: Retry!")
					time.sleep(60)
				else:
					break
		joblist=out.splitlines()
		n = len(joblist)
		print(str(n)+" jobs running")
		return n
			
	def transfer_job(self):
		out = None
		err = None
		command  = '{} -constraint \"JobStatus==4\" | grep {} | grep -v \"Total\"'.format(self.jobstat, self.user)
		while True:
			subpipe  = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)      
			out, err = subpipe.communicate()
			if err != None:
				print("Error while checking the number of finished jobs: Retry!")
				time.sleep(60)
			else:
				if self.htcerror in str(out):
					print("Error while checking the number of finished jobs: Retry!")
					time.sleep(60)
				else:
					break
		joblist=out.splitlines()
		for ij in joblist:
			jobid=float(ij.split()[0])
			print("Removing Job ID " + str(jobid))
			subprocess.call('{} {} '.format(self.jobtrans, jobid), shell=True)
			subprocess.call('{} {} '.format(self.jobrm, jobid), shell=True)
			time.sleep(self.waittime)

	def wait_job(self):
		while True:
			if self.njob() <= self.nmaxjobs:
				break
			self.transfer_job()
			time.sleep(120)
		return
			
	def running_job(self, name):
		out = None
		err = None
		command  = '{} | grep {} | grep -c {}'.format(self.jobstat, self.user, name)
		while True:
			subpipe  = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)      
			out, err = subpipe.communicate()
			if err != None:
				print("Error while checking the presence of running jobs: Retry!")
				time.sleep(60)
			else:
				if self.htcerror in str(out):
					print("Error while checking the presence of running jobs: Retry!")
					time.sleep(60)
				else:
					break
		retval = int(out)
		if retval>0:
			return True
		return False
		
	def submit_job(self, pfile, absdir, run_tag='', ofile='', seed=-1):
		# Set the odir, ofile, tag, and seed automatically
		odir = self.outdir
		pfile_base = os.path.splitext(os.path.basename(pfile))[0]
		if ofile == '':
		    ofile = pfile_base.replace('e2eprimary_', 'e2e_')
		    ofile += '.out'
		if run_tag == '':
		    run_tag = pfile_base
		if seed < 0:
		    tmp = pfile_base.replace(self.model, '')
		    list = re.split('\_|\.|run', tmp)
		    runs = [int(i) for i in list if unicode(i).isdecimal()]
		    if len(runs) >= 2:
		        seed = runs[-1] + runs[-2]*10000
		    elif len(runs) == 1:
		        seed = runs[0]
		    else:
		        seed = -1
		        
		# Define script arguments
		arguments="{} {} {} {} {} {}".format(self.model, run_tag, pfile, odir, ofile, seed)
		
		f = open(self.subfile, 'r')
		data = f.read()
		f.close()
		
		# Replace the parameters
		# Run Number
		# data = data.replace('$1', '{}'.format(run))
		# Model name
		data = data.replace('EXECUTABLE', self.shfile)
		data = data.replace('ARGUMENTS', arguments)
		data = data.replace('LOGDIR', self.logdir)
		data = data.replace('LOGTAG', run_tag)

		# Create dedicate .sh file
		filename = '{}/{}_{}.sub'.format(self.workdir, self.model, run_tag)
		f = open(filename, mode='w')
		f.write(data)
		f.close()
	
		ret = subprocess.call('{} {} '.format(self.jobsub, filename), shell=True)
		
		os.remove(filename)

		return
	
	def send_mail(self, n_submitted, n_total, n_days, start_time):
		# Send a daily report by email
		
		f = open("mail_{}.txt".format(self.user), 'r')
		data = f.read()
		f.close()
		
		data = data.replace('__SUBMITTED__', '{}'.format(n_submitted))
		data = data.replace('__TOTAL__', '{}'.format(n_total))
		data = data.replace('__NDAYS__', '{}'.format(n_days))
		data = data.replace('__START_DATE__', start_time.strftime('%Y/%m/%d'))
		
		filename = "./{}/mail_{}.txt".format(self.logdir, self.user)
		f = open(filename, 'w')
		f.write(data)
		f.close()
		
		ret = subprocess.call('cat {} | sendmail -i -t'.format(filename), shell=True)
		
		os.remove(filename)
				
		return
	
	def loop_jobs(self):

		if self.model is None:
			print("Please specify simulation model")
			sys.exit()
		if self.minrun is None:
			print("Please specify simulation first run number")
			sys.exit()
		if self.maxrun is None:
			print("Please specify simulation last run number")
			sys.exit()
		if self.indir is None:
			print("Please specify input directory")
			sys.exit()
		if self.outdir is None:
			print("Please specify output directory")
			sys.exit()

		# absolute path
		absdir = os.path.abspath(self.indir)
		
		# list of primary files
		tmp = absdir
		if os.path.isdir(absdir):
		    tmp += '/*'
		    ##
		else:
		    print('Error: Provide a directory path of primary files.')
		    return
		
		pfilelist = glob.glob(tmp+'*.txt')
		pfilelist.sort()
			        
		# create auxiliary directories
		self.workdir = self.outdir+'/work/' #Folder from where jobs are launched
		self.logdir = self.outdir+'/log/' #Folder where submission log is saved

		self.make_directories()
		self.setup_logger()
	
		# loop of files
		n_submitted = 0
		n_total = len(pfilelist)
		
		start_time = datetime.now()
		clock_time = start_time
		for pfile in pfilelist:
			run=int(pfile[-14:-8])
			if run < self.minrun or run > self.maxrun:
				continue;
			#Skip empty runs
			if self.empty(pfile):
				continue;

			#Job submission
			current_time = datetime.now()
			self.transfer_job()
			self.wait_job()
			if self.running_job(pfile):
				self.logger.info('Job ' + pfile + ' is already running!')
				time.sleep(self.waittime)
				continue
			self.logger.info('Processing file ' + pfile)
			self.submit_job(pfile, absdir)
			n_submitted += 1
			time.sleep(self.waittime)
			
			##Mail sending
			#if current_time > clock_time + timedelta(days=7):
			#	send_mail(n_submitted, n_total, (current_time - start_time).days, start_time)
			#	clock_time = datetime.now()

#!/usr/bin/python3

from jobcontrol import JobController

###################################################
### Nominal Position Any-Hit Simulation Any-Hit ###
###################################################

#generator_model="QGSJET2_04"
generator_model="EPOSLHC"
simulation_minrun=1
simulation_maxrun=1000
#NB: PFilter is the output of generate_primarys which has DoubleArm as input
input_directory="/storage/gpfs_data/lhcf/simulation/LHC2022/PFilter/v1_20240614/"+generator_model+"/"
output_directory="/storage/gpfs_data/lhcf/simulation/LHC2022/End2End/v1_20240614/Arm2_Center/"+generator_model+"/"
nmaxjobs_limit=2000
waittime_sleep=5

#######################################
#### Nominal Full Simulation 4-Hits ###
#######################################
#
##generator_model="QGSJET2_04"
#generator_model="EPOSLHC"
#simulation_minrun=20000
#simulation_maxrun=29999
##NB: PFilter is the output of generate_primarys which has DoubleArm as input
#input_directory="/storage/gpfs_data/lhcf/simulation/LHC2022/PFilter/v1_mass_4hits/"+generator_model+"/set-"+str(simulation_minrun)+"-"+str(simulation_maxrun)+"/"
#output_directory="/storage/gpfs_data/lhcf/simulation/LHC2022/End2End/v1_mass_4hits/"+generator_model+"/set-"+str(simulation_minrun)+"-"+str(simulation_maxrun)+"/"
#nmaxjobs_limit=1000
#waittime_sleep=1

# main
if __name__ == '__main__':

    con = JobController() 

    con.set_model(generator_model)
    con.set_minrun(simulation_minrun)
    con.set_maxrun(simulation_maxrun)
    con.set_input(input_directory)
    con.set_output(output_directory)
    con.set_nmaxjobs(nmaxjobs_limit)
    con.set_waittime(waittime_sleep)

    con.loop_jobs()


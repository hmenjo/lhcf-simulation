#!/bin/bash
#------ pjsub option --------#
#PJM -L "rscgrp=B"
#PJM -L "vnode=1"
#PJM -L "vnode-core=1"
#PJM -j
#PJM -o "/disk/lhcf/user/simulation_Run3/Data/End2End/v1/Arm1_Center/EPOSLHC/log/log_${__FILE_TAG__}.out"
#------- Program execution -------#
# History
# 

if [[ "$HOSTNAME" == *lhcf* ]]; then 
# For lhcfs2
   LHCF_SIM_BASE="/disk/lhcf/user/simulation_Run3/lhcf-simulation/"
   source ${LHCF_SIM_BASE}/End2End/setup_epics_nagoya.sh
   DATADIR="/disk/lhcf/user/simulation_Run3/Data/"
else 
# For ICRR 
   LHCF_SIM_BASE="/disk/lhcf/user/simulation_Run3/lhcf-simulation/"
   source ${LHCF_SIM_BASE}/End2End/setup_epics_icrr.sh
   DATADIR="/disk/lhcf/user/simulation_Run3/Data/"
fi

MODEL="EPOSLHC"

WORKDIR="${DATADIR}/End2End/tmp/${__FILE_TAG__}"
INPUT_DIR="${__INPUT_DIR__}"
INPUT_FILENAME="${__INPUT_FILE__}"
OUTPUT_DIR="${DATADIR}/End2End/v1/Arm1_Center/${MODEL}/"
OUTPUT_FILENAME="${__OUTPUT_FILE__}"
INPUT_COMPLETED="${OUTPUT_DIR}/primary_completed/"

# Setup 
mkdir -p ${WORKDIR}
mkdir -p ${OUTPUT_DIR}/log
mkdir -p ${OUTPUT_DIR}/old
mkdir -p ${INPUT_COMPLETED}
cd ${WORKDIR}

date

# make symbolic link for input file 
if [ -f "$INPUT_FILENAME" ];then
    ln -s ${INPUT_FILENAME} +primary
else 
    echo "ERROR: No input file was found"
    exit 
fi

# Check presence of old file
if [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME.gz" ];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/
    echo "mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/"
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
elif [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME" ];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME $OUTOUT_DIR/old
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
fi

END2END_DIR="${LHCF_SIM_BASE}/End2End/src/" 
END2END_BIN="sepicsPCLinuxIFC64"
# copy files to the working directory
cp $END2END_DIR/$END2END_BIN ./
cp $END2END_DIR/FirstInput_arm1.LHC2022 ./
cp $END2END_DIR/param ./
cp $END2END_DIR/sepicsfile_arm1.gso.lhc ./tmp_sepicsfile
cp $END2END_DIR/epicsfile* ./
cp -r $END2END_DIR/cnfg ./
cp -r $END2END_DIR/src ./

# replace the random seed 
sed -e "s/-123456/${__SEED__}/g" tmp_sepicsfile  > sepicsfile_arm1.gso.lhc

./$END2END_BIN < FirstInput_arm1.LHC2022 > $OUTPUT_DIR/$OUTPUT_FILENAME 
RETVAL=$?

if [ $RETVAL -eq 0 ]; then 
    # gzip the output file
    gzip $OUTPUT_DIR/$OUTPUT_FILENAME
    mv $INPUT_FILENAME $INPUT_COMPLETED
    echo "Completed"
else 
    echo "Not completed"
fi 

# remove the directory 
rm -rf $WORKDIR

date


import subprocess
import time
if __name__ == '__main__':
    
    #data_dir = '/disk/lhcf/user/simulation_Run3/Data/End2End/v1/Arm1_Center/EPOSLHC'
    data_dir = '/disk/lhcf/user/simulation_Run3/Data/End2End/v1/Arm1_Center/QGSJET2_04'
    dest_dir = 'lhcf@lhcfs2:/mnt/lhcfnas3/data/Simulation/Op2022/Data/End2End/v1/Arm1_Center/'

    while True:
        command = f'rsync -av -e ssh --include="*.out.gz" --exclude="*/old/*" {data_dir} {dest_dir}'
        subprocess.run(command, shell=True)
        #print(command)
        print('wait for 1 day')
        time.sleep(24*60*60)

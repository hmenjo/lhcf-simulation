#include <string.h>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

// ------------------------------------------------------------
class Particle {
 public:
  int pcode;
  int psubcode;
  int pcharge;
  double ke;
  double x;
  double y;
  double z;
  double wx;
  double wy;
  double wz;
  double tag;

 public:
  Particle(int a_pcode, int a_psubcode, int a_pcharge, double a_ke, double a_x,
           double a_y, double a_z, double a_wx, double a_wy, double a_wz,
           double a_tag)
      : pcode(a_pcode),
        psubcode(a_psubcode),
        pcharge(a_pcharge),
        ke(a_ke),
        x(a_x),
        y(a_y),
        z(a_z),
        wx(a_wx),
        wy(a_wy),
        wz(a_wz),
        tag(a_tag) {
    ;
  }

  string Print(bool mhseparation = false) {
    ostringstream sout;
    sout.unsetf(ios::fixed);
    sout << setw(2) << pcode << " " << setw(2) << psubcode << " " << setw(2)
         << pcharge << " " << setw(12) << setprecision(10) << ke << " " << fixed
         << setw(9) << setprecision(5) << x << " " << setw(9) << setprecision(5)
         << y << " " << setw(9) << setprecision(5) << z << " " << setw(12)
         << setprecision(9) << wx << " " << setw(12) << setprecision(9) << wy
         << " " << setw(12) << setprecision(9) << wz << " ";
    if (mhseparation) {
      sout << setw(12) << setprecision(2) << tag << " " << endl;
    } else {
      sout << setw(12) << (int)tag << " " << endl;
    }
    return sout.str();
  }

  // For Sorting by Kinetic Energy
  bool operator<(const Particle &another) const { return ke < another.ke; }
};

// -------------------------------------------------------
class ParticleList {
 public:
  int fNevent;
  int fNparticle;
  int fNoutevents;
  vector<Particle> fParticleList;
  bool fMhSeparation;

 public:
  ParticleList()
      : fNevent(0), fNparticle(0), fNoutevents(0), fMhSeparation(false) {
    Clear();
  }

  int GetNpartciles() { return fNparticle; }
  void SetMultiHitSeparation(bool f = true) { fMhSeparation = f; }
  void Clear() {
    fNparticle = 0;
    fParticleList.clear();
  }
  void Add(int eventid, int a_pcode, int a_psubcode, int a_pcharge, double a_ke,
           double a_x, double a_y, double a_z, double a_wx, double a_wy,
           double a_wz, int a_tag) {
    double tag =
        a_tag +
        0.0001;  // 0.0001 is for safety in the conversion from double to int
    fNparticle++;
    fParticleList.push_back(Particle(a_pcode, a_psubcode, a_pcharge, a_ke, a_x,
                                     a_y, a_z, a_wx, a_wy, a_wz, tag));
  }
  void Sort() {
    // Sort by Kinetic Energy
    sort(fParticleList.rbegin(), fParticleList.rend());
    // Re-assign the tag (user ID)
    if (fMhSeparation) {
      for (int i = 0; i < fNparticle; i++) {
        if (i < 100) {
          fParticleList[i].tag += 0.01 * (i + 1);
        } else {
          fParticleList[i].tag += 0.99;
        }
      }
    }
  }
  void Print(ostream &sout) {
    bool bcheck = false;
    // Increment the event ID
    fNevent++;

    if (fMhSeparation && fParticleList.size() == 0) {
      return;
    }

    for (int i = 0; i < fNparticle; i++) {
      bcheck = false;
      sout << fParticleList[i].Print(fMhSeparation);

      if (fMhSeparation && fParticleList[i].ke > 20) {
        sout << endl;
        fNoutevents++;
        bcheck = true;
      }
    }
    // End of event
    if (!bcheck) {
      sout << endl;
      fNoutevents++;
    }
    return;
  }
};

// -------------------------------------------------
class EventList {
 public:
  int f_nevents;
  vector<ParticleList *> f_events;
  int f_detector;
  double f_shiftx;  // beam center shift x,y on the "Detector plain" (not on
                    // TAN) with cm.
  double f_shifty;  // The x,y should be the beam center position on the
                    // detector coordinate.
  double f_detectorpos;  // detector y pos on "Detector plain" (not on TAN)
  bool f_beampipecut;
  bool f_tanwallcut;
  bool f_inverse;
  bool f_bkgcut;
  bool f_mhseparation;

 public:
  EventList();
  void Set(int detector, double shiftx, double shifty, double pos,
           double crossingangle = 145.) {
    // crossingangle : urad and downgoing
    // const double distance_fromIP = 13980.;  // distance from TAN to Detector in cm.
    f_detector = detector;
    f_shiftx = shiftx;
    f_shifty = shifty;
    // f_shifty = shifty - tan(crossingangle*1.E-6)*distance_fromIP;
    f_detectorpos = pos;
  }
  int ReadFile(const string filename);
  int WriteResults(const string filename, bool fileseparation, int event_limit, bool verbosity);
  int ApplyEventSelection_AnyHit();
  int ApplyEventSelection_4Hits();
  int ApplyEventSelection_K0Short();

 private:
  int cut_beampipe(double x, double y) {
    double r2;
    r2 = (x / 106.2) * (x / 106.2) + (y / 43.97) * (y / 43.97);
    if (r2 > 1.0)
      return 0;
    else
      return 1;
  }
};

EventList::EventList()
    : f_nevents(0),
      f_detector(0),
      f_shiftx(0.),
      f_shifty(0.),
      f_detectorpos(0.),
      f_beampipecut(false),
      f_tanwallcut(false),
      f_inverse(false),
      f_bkgcut(false),
      f_mhseparation(false) {
  ;
}

int EventList::ReadFile(const string filename) {
  ifstream fin(filename.c_str());
  if (!fin) {
    cout << "Error: Could not open " << filename << endl;
    return -1;
  }

  // File read
  f_nevents = 0;
  ParticleList *plist = new ParticleList();
  plist->SetMultiHitSeparation(f_mhseparation);
  char a[500];
  int ndata = 0;
  int ncdata = 0;
  int event, tag;
  int pcode, psubcode, pcharge;
  double ke, r, x, y, z;
  double t, wx, wy, wz;
  double ox, oy;
  while (fin.getline(a, 500)) {
    // *** delete header info ***
    if (strlen(a) > 5 && strlen(a) < 40) {
      continue;
    }
    // *** identiry a line feed ***
    if (strlen(a) < 5) {
      // End of the event (one collision)
      // Sort by Kinetic Energy
      plist->Sort();
      ndata = 0;
      ncdata = 0;
      f_events.push_back(plist);
      plist = new ParticleList();
      plist->fNevent = ++f_nevents;
      plist->SetMultiHitSeparation(f_mhseparation);
      continue;
    }
    // *** Skip additional information lines
    if (a[0] == '#') {
      continue;
    }
    // *** get data ***
    sscanf(a, "%d%d%d%d%d%lf%lf%lf%lf%lf%lf%lf%lf%lf", &event, &tag, &pcode,
           &psubcode, &pcharge, &ke, &r, &x, &y, &z, &t, &wx, &wy, &wz);
    ndata++;
    ox = x;
    oy = y;

    // *** convert for each arm ***
    if (f_detector == 1) {
      if (f_inverse == false) {
        if (z < 0.) continue;
        z = 0.;
      } else {
        if (z > 0.) continue;
        x = -1. * x;
        wx = -1. * wx;
        z = 0.;
        wz = -1. * wz;
      }

      x += f_shiftx;
      y += f_shifty;
    } else if (f_detector == 2) {
      if (f_inverse == false) {
        if (z > 0.) continue;
        x = -1. * x;
        wx = -1. * wx;
        z = 0.;
        wz = -1. * wz;
      } else {
        if (z < 0.) continue;
        z = 0.;
      }

      x += f_shiftx;
      y += f_shifty;
    }

    // *** cut ***
    ndata++;
    if (sqrt(x * x + y * y) > 10.) {
      continue;
    }
    if (f_beampipecut == true && cut_beampipe(ox * 10., oy * 10.) == 0) {
      continue;
    }
    if (f_tanwallcut == true && fabs(x) > 4.6) {
      continue;
    }
    // if(ke < 10.){continue;}
    if (f_bkgcut == true && tag < 0) {
      continue;
    }
    ncdata++;

    // Fill to the list
    plist->Add(event, pcode, psubcode, pcharge, ke, x, y, z, wx, wy, wz, tag);

    // if(event>10) break;
  }
  fin.close();

  // Print the information

  cout << "INPUT ----------------------------------------\n"
       << "Read file: " << filename << endl
       << "  Number of events found in the file = " << f_nevents << endl
       << "  Number of particles before selections = " << ndata << endl
       << "  Number of particles after selections = " << ncdata << endl;

  return 0;
}

int EventList::WriteResults(const string filename, bool fileseparation, int event_limit, bool verbosity) {
   const string header =
      "# mul sub KE xyz dir  user disk49 "
      "\n#---------------------------------\n";

  int iev = 0;
  int subrun = 1;
  char ofilename[256];
  int evlimit = fileseparation ? event_limit : 1000000000;

  if (verbosity>0)
	cout << "OUTPUT -------------------------------------\n"
	     << "File separation is " << (fileseparation ? "ON" : "OFF") << endl;
  while (1) {
    if (fileseparation)
      sprintf(ofilename, filename.c_str(), subrun++);
    else
      strcpy(ofilename, filename.c_str());

    ofstream fout(ofilename);
    fout.setf(ios::right);
    fout << header;
    int nev = 0;
    int np = 0;
    for (int i = 0; i < evlimit; i++) {
      f_events[iev]->Print(fout);
      np += f_events[iev]->GetNpartciles();
      iev++;
      nev++;
      if (iev == f_nevents) break;
    }
    fout.close();
    if (verbosity>0)
	    cout << "Output file : " << ofilename << endl
		 << "  Number of collision events in the file = " << nev << endl
	         << "  Number of particles in the file = " << np << endl;
    if (iev >= f_nevents) break;
  }
  return 0;
}

int EventList::ApplyEventSelection_AnyHit() {
  const double distance =
      14050. - 13980;  // distance from TAN to Detector in cm.
  const double sqrt2 = sqrt(2.);
  const double margin = 0.1;  // mergin in cm

  int nev = 0;
  int nev_sel = 0;

  for (int iev = 0; iev < f_nevents; iev++) {
    nev++;

    int nhit = 0;
    for (int ip = 0; ip < f_events[iev]->fNparticle; ip++) {
      double x, y;  // pos on the LHCf plain
      double tmpx, tmpy, x2, y2;

      x = f_events[iev]->fParticleList[ip].x +
          distance * f_events[iev]->fParticleList[ip].wx /
              f_events[iev]->fParticleList[ip].wz;
      y = f_events[iev]->fParticleList[ip].y +
          distance * f_events[iev]->fParticleList[ip].wy /
              f_events[iev]->fParticleList[ip].wz;

      // correction of detector position in y
      y -= f_detectorpos;

      // Arm1
      if (f_detector == 1) {
        // check hit in TS
        tmpx = x / sqrt2 - y / sqrt2;
        tmpy = x / sqrt2 + y / sqrt2;
        if (fabs(tmpx) < 1.0 + margin && fabs(tmpy) < 1.0 + margin) nhit++;
        // check hit in TL
        x2 = x;
        y2 = y - 3. * sqrt2 - 0.5;  // to TL center origin coordinate
        tmpx = x2 / sqrt2 - y2 / sqrt2;
        tmpy = x2 / sqrt2 + y2 / sqrt2;
        if (fabs(tmpx) < 2.0 + margin && fabs(tmpy) < 2.0 + margin) nhit++;
      }
      // Arm2 (Work only for the upgraded detector. > 2015)
      else if (f_detector == 2) {
        // check hit in TS
        tmpx = x + 0.80;
        tmpy = y + 1.25;
        if ((tmpx > 0.00 - margin && tmpx < 2.50 + margin) &&
            (tmpy > 0.00 - margin && tmpy < 2.50 + margin))
          nhit++;
        // check hit in TL
        tmpx = x + 0.80 + 0.18 + 3.20;
        tmpy = y - 1.25 - 0.18;
        if ((tmpx > 0.00 - margin && tmpx < 3.20 + margin) &&
            (tmpy > 0.00 - margin && tmpy < 3.20 + margin))
          nhit++;
      }
    }

    if (nhit == 0) {
      f_events[iev]->Clear();
      continue;
    }
    nev_sel++;
  }
  cout << "EVENT SELECTION ----------------------------------\n"
       << "Number of events = " << nev << endl
       << "Number of selected events = " << nev_sel << endl;
  return 0;
}

// 24,Jan,2025 added member function "ApplyEventSelection_4Hits" by Eugenio
// select only 4 hits event for ML development
int EventList::ApplyEventSelection_4Hits() {
	const double distance = 14050. - 13980;  // distance from TAN to Detector in cm.
	const double sqrt2 = sqrt(2.);
	const double margin = 0.1;

	int nev = 0;
	int nev_sel = 0;

	for (int iev = 0; iev < f_nevents; iev++) {
		nev++;

		int nhit = 0;
		for (int ip = 0; ip < f_events[iev]->fNparticle; ip++) {
			double x, y;  // pos on the LHCf plain
			double tmpx, tmpy, x2, y2;

			x = f_events[iev]->fParticleList[ip].x +
					distance * f_events[iev]->fParticleList[ip].wx;
			y = f_events[iev]->fParticleList[ip].y +
					distance * f_events[iev]->fParticleList[ip].wy;

			// correction of detector position in y
			y -= f_detectorpos;

			// Arm1
			// Arm1

			if (f_detector == 1) {
				// check hit in TS

				tmpx = x / sqrt2 - y / sqrt2;
				tmpy = x / sqrt2 + y / sqrt2;
				if (fabs(tmpx) < 1.0 + margin && fabs(tmpy) < 1.0 + margin) nhit++;
				// check hit in TL

				x2 = x;
				y2 = y - 3. * sqrt2 - 0.5;  // to TL center origin coordinate

				tmpx = x2 / sqrt2 - y2 / sqrt2;
				tmpy = x2 / sqrt2 + y2 / sqrt2;
				if (fabs(tmpx) < 2.0 + margin && fabs(tmpy) < 2.0 + margin) nhit++;
			}
			// Arm2 (Work only for the upgraded detector. > 2015)

			else if (f_detector == 2) {
				// check hit in TS
				tmpx = x + 0.80;
				tmpy = y + 1.25;
				if ((tmpx > 0.00 - margin && tmpx < 2.50 + margin) &&
						(tmpy > 0.00 - margin && tmpy < 2.50 + margin))
					nhit++;
				// check hit in TL
				tmpx = x + 0.80 + 0.18 + 3.20;
				tmpy = y - 1.25 - 0.18;
				if ((tmpx > 0.00 - margin && tmpx < 3.20 + margin) &&
						(tmpy > 0.00 - margin && tmpy < 3.20 + margin))
					nhit++;
			}
		}

		if (nhit != 4) {
			f_events[iev]->Clear();
			continue;
		}
		nev_sel++;
	}
	cout << "4 Hits EVENT SELECTION ----------------------------------\n"
			<< "Number of events = " << nev << endl
			<< "Number of selected events = " << nev_sel << endl;
	return 0;
}

// 25,Apr,2024 added member function "K0ShortSelection" by Kosuke Kinoshita.
// use with the pure K0s MC.
int EventList::ApplyEventSelection_K0Short() {
  const double distance =
      14050. - 13980;  // distance from TAN to Detector in cm.
  const double sqrt2 = sqrt(2.);
  const double margin = 0.1;

  int nev = 0;
  int nev_sel = 0;

  for (int iev = 0; iev < f_nevents; iev++) {
    nev++;

    int nhit = 0;
    for (int ip = 0; ip < f_events[iev]->fNparticle; ip++) {
      double x, y;  // pos on the LHCf plain
      double tmpx, tmpy, x2, y2;

      if (f_events[iev]->fParticleList[ip].pcode == 1 &&  // event selection
          f_events[iev]->fParticleList[ip].psubcode == 2 &&
          f_events[iev]->fParticleList[ip].ke > 100) {
        x = f_events[iev]->fParticleList[ip].x +
            distance * f_events[iev]->fParticleList[ip].wx;
        y = f_events[iev]->fParticleList[ip].y +
            distance * f_events[iev]->fParticleList[ip].wy;

        // correction of detector position in y
        y -= f_detectorpos;

        // Arm1
        // Arm1

        if (f_detector == 1) {
          // check hit in TS

          tmpx = x / sqrt2 - y / sqrt2;
          tmpy = x / sqrt2 + y / sqrt2;
          if (fabs(tmpx) < 1.0 + margin && fabs(tmpy) < 1.0 + margin) nhit++;
          // check hit in TL

          x2 = x;
          y2 = y - 3. * sqrt2 - 0.5;  // to TL center origin coordinate

          tmpx = x2 / sqrt2 - y2 / sqrt2;
          tmpy = x2 / sqrt2 + y2 / sqrt2;
          if (fabs(tmpx) < 2.0 + margin && fabs(tmpy) < 2.0 + margin) nhit++;
        }
        // Arm2 (Work only for the upgraded detector. > 2015)

        else if (f_detector == 2) {
          // check hit in TS
          tmpx = x + 0.80;
          tmpy = y + 1.25;
          if ((tmpx > 0.00 - margin && tmpx < 2.50 + margin) &&
              (tmpy > 0.00 - margin && tmpy < 2.50 + margin))
            nhit++;
          // check hit in TL
          tmpx = x + 0.80 + 0.18 + 3.20;
          tmpy = y - 1.25 - 0.18;
          if ((tmpx > 0.00 - margin && tmpx < 3.20 + margin) &&
              (tmpy > 0.00 - margin && tmpy < 3.20 + margin))
            nhit++;
        }
      }

      //Eugenio: I do not understand how this can work here!
      if (nhit != 4) {
        f_events[iev]->Clear();
        continue;
      }
    }
    nev_sel++;
  }
  cout << "K0s EVENT SELECTION ----------------------------------\n"
       << "Number of events = " << nev << endl
       << "Number of selected events = " << nev_sel << endl;
  return 0;
}

// -------------------------------------------------

// Main --------------------------------------------
int main(int argc, char **argv) {
  // int detector = 0;
  string inputfilename;
  string outputfilename;
  bool fileseparation = false;
  int limit_nevent = 0;
  bool eventselection_anyhit = false;
  bool eventselection_4hits = false;
  bool eventselection_purek0s = false;
  int verbosity = 1;
  EventList evlist;

  for (int i = 0; i < argc; i++) {
    string ss = argv[i];

    // if (ss == "-d") {
    //   if (strcmp(argv[i + 1], "arm1") == 0 || strcmp(argv[i + 1], "Arm1") ==
    //   0) {
    //     detector = 1;
    //   } else if (strcmp(argv[i + 1], "arm2") == 0 || strcmp(argv[i + 1],
    //   "Arm2") == 0) {
    //     detector = 2;
    //   } else {
    //     cerr << "Incorrect value: -d " << argv[i + 1] << endl;
    //     return -1;
    //   }
    //   i++;
    // }
    if (ss == "-i" || ss == "--input") {
      inputfilename = argv[++i];
    }
    if (ss == "-o" || ss == "--output") {
      outputfilename = argv[++i];
    }
    if (ss == "-wBPCut") {
      evlist.f_beampipecut = true;
    }
    if (ss == "-woBPCut") {
      evlist.f_beampipecut = false;
    }
    if (ss == "-wTANWCut") {
      evlist.f_tanwallcut = true;
    }
    if (ss == "-wBKGCut") {
      evlist.f_bkgcut = true;
    }
    if (ss == "--inverse") {
      evlist.f_inverse = true;
    }
    if (ss == "--mhseparation") {
      evlist.f_mhseparation = true;
    }
    if (ss == "--fileseparation") {
      fileseparation = true;
      limit_nevent = atoi(argv[++i]);
    }
    if (ss == "--selection_anyhit") {
      eventselection_anyhit = true;
    }
    if (ss == "--selection_4hits") {
      eventselection_4hits = true;
    }
    if (ss == "--selection_purek0s") {
      eventselection_purek0s = true;
    }
    if (ss == "--Op2015_Arm1_Center") {
      evlist.Set(1, 0.0, 0.0, -2.0, 145.);
    }
    if (ss == "--Op2015_Arm1_5mmHigh") {
      evlist.Set(1, 0.0, 0.0, -1.5, 145.);
    }
    if (ss == "--Op2015_Arm2_Center") {
      evlist.Set(2, 0.0, 0.0, -2.0, 145.);
    }
    if (ss == "--Op2015_Arm2_5mmHigh") {
      evlist.Set(2, 0.0, 0.0, -1.5, 145.);
    }
    if (ss == "--Op2022_Arm1_Center") {
      evlist.Set(1, -0.127, 0.017, -2.0,
                 145.);  // 25 May 2024 Based on Kondo-san's result
    }
    if (ss == "--Op2022_Arm1_5mmHigh") {
      evlist.Set(1, -0.127, 0.017, -1.5, 145.);  // Temporal, To do
    }
    if (ss == "--Op2022_Arm2_Center") {
      evlist.Set(2, -0.332200, -0.075239, -2.0,
                 145.);  // 3 May 2024 - Fill 8178 - SubFill A
    }
    if (ss == "--Op2022_Arm2_5mmHigh") {
      evlist.Set(2, 0.0, 0.0, -1.5, 145.);
    }
    if (ss == "-v" || ss == "--verbosity") {
      verbosity = atoi(argv[++i]);
    }

    if (argc == 1 || strcmp(argv[i], "-h") == 0 ||
        strcmp(argv[i], "--help") == 0) {
      cout << " primaryfilter_v3 "
              "*************************************************\n"
           << " ./bin/primaryfilter_v3 --Op2022_Arm1_Center -i [inputfile] -o "
              "[outputfile] \n"
           << " \n"
           << " Suggested option for Op2022 MC\n"
           << " ./bin/primaryfilter_v3 --Op2022_Arm1_Center -i "
              "darm_run00001.out -o "
              "e2eprimary_run00001_%03d.out --mhseparation\n"
           << " --fileseparation 1000 --selection_anyhit\n"
           << "\n"
           << " Options)\n"
           //           << " -d      : detector [arm1 or arm2]\n"
           << "type sel:  --Op2022_Arm1_Center, --Op2022_Arm1_5mmHigh, "
              "--Op2022_Arm2_Center, "
              "--Op2022_Arm2_5mmHigh\n"
           << "           --Op2015_Arm1_Center, --Op2015_Arm1_5mmHigh, "
              "--Op2015_Arm2_Center, "
              "--Op2015_Arm2_5mmHigh\n"
           << " -i      : input file name (output of DoubleArm)\n"
           << " -o      : output file name (\"+primary\")\n"
           << " -wBPCut : particle selection with ellipse beam-pipe\n "
           << "         : Use this cut for outputs of DoubleArm without "
              "matter.\n"
           << " -wTANWCut: particle selection hitting in the tan wall cut "
              "(abs(x)<4.8cm)\n"
           << " -wBKGCut:  particle selection with Backgound Cut.\n"
           << " --inverse:  inverse input for the detector. (z_pos -> Arm2, "
              "z_nag -> Arm1)\n"
           << " --mhseparation: generate event for individual incident "
              "particle. (default)\n"
           << " --no_mhseparation: mhseparation option to off\n"
           << " --fileseparation (limit): Save results to files separately for "
              "every (limit) "
              "events. \n"
           << "             in this case, please specify the output filename "
              "as output_\%03d.root\n"
           << " --selection_anyhit: Select events with any hit in LHCf "
              "calorimeters.\n"
           << " --selection_4hits: Select events with 4 hits in LHCf "
              "calorimeters.\n"
           << " --selection_purek0s: Select events that decayed from K0s to 4 "
              "photons.\n"
           << " -v or --verbosity: from 0 to 1.\n"
           << endl;
      return 0;
    }
  }

  // // Check the options
  // if (detector == 0) {
  //   cerr << "Please specify the detector or data type" << endl;
  //   return -1;
  // }

  // const double arm1_shiftx = 0.094;
  // const double arm1_shifty = 0.11;
  // const double arm2_shiftx = -0.3529;
  // const double arm2_shifty = -0.0579;

  if (evlist.f_detector == 0) {
    cerr << "ERROR: specify the data set" << endl;
    return -1;
  }

  // Read input file and store the data to a vector array.
  if (evlist.ReadFile(inputfilename) < 0) return -1;
  // Event Selection
  if (eventselection_anyhit) evlist.ApplyEventSelection_AnyHit();
  if (eventselection_4hits) evlist.ApplyEventSelection_4Hits();
  if (eventselection_purek0s) evlist.ApplyEventSelection_K0Short();
  // Write to a file.
  evlist.WriteResults(outputfilename, fileseparation, limit_nevent, verbosity);

  return 0;
}

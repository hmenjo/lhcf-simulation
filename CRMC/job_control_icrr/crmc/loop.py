import subprocess
import time
import os
from logging import getLogger, StreamHandler, Formatter, FileHandler, DEBUG

# History 
#    Original file : /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/run/loop.py
#    2022 Nov. 11th: Copied from menjo-san's directory and modified. 

logger = getLogger(__name__)

def setup_logger (logfile='submitlog.txt'):
    logger = getLogger(__name__)
    logger.setLevel(DEBUG)

    sh = StreamHandler()
    sh.setLevel(DEBUG)
    formatter = Formatter('%(asctime)s: %(message)s')
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    fh = FileHandler(logfile) #fh = file handler
    fh.setLevel(DEBUG)
    fh_formatter = Formatter('%(asctime)s : %(message)s')
    fh.setFormatter(fh_formatter)
    logger.addHandler(fh)
    return logger
    

def njob () :
    ret = subprocess.check_output('pjstat | grep menjo | wc', shell=True)
    vals = ret.split()
    n = int(vals[0])
    return n

def waitjob (limit=500) :
    while True :
        if njob() <= limit :
            break
        time.sleep(10)
    return

def submit_job (orgfile, run, model='EPOSLHC', tag='job'):

    with open(orgfile, 'r') as f:
        data = f.read()
    
    # Run Number    
    data = data.replace('$1','{}'.format(run))

    # Model name
    data = data.replace('EPOSLHC',model)

    print(model)
    # CRMC option
    if model=='EPOSLHC':
        data = data.replace('-m0','-m0') # for crmc 1.8
        data = data.replace('-m 0','-m 0') # for crmc 2
    elif model == 'QGSJET2_04':
        data = data.replace('-m0','-m7')
        data = data.replace('-m 0','-m 7')
        daat = data.replace('QGSJET2_FLAG=""', 'QGSJET2_FLAG="-p"')
    elif model == 'DPMJET3_2019':
        data = data.replace('-m0','-m12')
        data = data.replace('-m 0','-m 12')
    elif model == 'SIBYLL2.3d':
        data = data.replace('-m0','-m6')
        data = data.replace('-m 0','-m 6')
    elif model == 'QGSJET3_01':
        data = data.replace('-m0','-m13')
        data = data.replace('-m 0','-m 13')

    filename = './run/{}_run{}.sh'.format(model,run) 
    with open(filename,mode='w') as f:
        f.write(data)

    ret = subprocess.call('pjsub {} '.format(filename), shell=True)
    logger.info('submit {} ({})'.format(filename, orgfile));
    return 

def loop_one_run (orgfile, run) :
    step = 50000
    for i in range(0,40):
        waitjob()
        submit_job(orgfile, run, i*step, (i+1)*step-1)
    return 

#### send main after the submission of jobs 
def send_mail (model = 'EPOSLHC') :
    with open("mail_menjo.txt", 'r') as f:
        data = f.read()
    data = data.replace('EPOSLHC', '{}'.format(model))
    filename = "./submitlog/mail_menjo_{}.txt".format(model)
    with open(filename, 'w') as f:
        f.write(data)
    ret = subprocess.call('cat {} | sendmail -i -t'.format(filename), shell=True)
    return

#### Loop for specific data sets
                        
def do_EPOS (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
    for run in range(srun,erun+1):
         waitjob(limit)
         submit_job (orgfile, run, model='EPOSLHC')
    send_mail( model='EPOSLHC')
    return
   
def do_QGS (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
    for run in range(srun,erun+1):
         waitjob(limit)
         submit_job (orgfile, run, model='QGSJET2_04')
    send_mail( model='QGSJET2_04')
    return

def do_SIBYLL (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
    for run in range(srun,erun+1):
         waitjob(limit)
         submit_job (orgfile, run, model='SIBYLL2.3d')
    send_mail( model='SIBYLL2.3d')
    return
   
def do_DPM (srun=1, erun=1000., limit = 500, orgfile = 'run.sh'):
    for run in range(srun,erun+1):
         waitjob(limit)
         submit_job (orgfile, run, model='DPMJET3_2019')
    send_mail( model='DPMJET3_2019')
    return

def do_QGS3 (srun=1, erun=1000, limit = 500, orgfile = 'run_crmc2.sh'):
    for run in range(srun,erun+1):
         waitjob(limit)
         submit_job (orgfile, run, model='QGSJET3_01')
    send_mail( model='QGSJET3_01')
    return
   
#### main
if __name__ == '__main__' :
    os.makedirs('./run/', exist_ok=True)   
    os.makedirs('./submitlog/', exist_ok=True)   
    setup_logger(logfile="./submitlog/log.txt")
  
    #do_EPOS(101,1000)
    #do_QGS(101,1000) 
    #do_SIBYLL(1,100, limit = 600)
    #do_DPM(1,100, limit = 600)
    do_QGS3(1,100, limit = 650) 

    #do_EPOS(1,1000,orgfile='run_convert_only.sh')
    #do_QGS(1,1000,orgfile='run_convert_only.sh') 
 
    #do_EPOS(1, 2000, limit=100, orgfile='run_forwPipe.sh') 

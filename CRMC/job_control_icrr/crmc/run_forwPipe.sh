#!/bin/bash
#------ pjsub option --------#
#PJM -L "rscgrp=A"
#PJM -L "vnode=1"
#PJM -L "vnode-core=1"
#PJM -j
#PJM -o "/disk/lhcf/user/simulation_Run3/Data/crmc_1.8.0/hepmc_forwPipe/EPOSLHC/log/log_run$1.out"
#------- Program execution -------#
# History
#    2022 Nov. 11th : copied from the original file in /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/run/run.sh and removed script related to conversion and transports. 

RUN=$1

source /disk/lhcf/user/simulation_Run3/lhcf-simulation/CRMC/setup_crmc_icrr.sh

MODEL="EPOSLHC"
MODELOP="-m0"
DATADIR="/disk/lhcf/user/simulation_Run3/Data/crmc_1.8.0/"
WORKDIR="${DATADIR}/crmc_tmp/${MODEL}_${RUN}/"
OUTDIR_CRMC="${DATADIR}/hepmc_forwPipe/${MODEL}/"
OUTDIR_OLD="${DATADIR}/old/hepmc_forwPipe/${MODEL}/"
OUTDIR_CONV="${DATADIR}/hepmc2gencol_forwPipe/${MODEL}/"

# Setup 
mkdir -p ${WORKDIR}
mkdir -p ${OUTDIR_CRMC}/log
mkdir -p ${OUTDIR_OLD}
mkdir -p ${OUTDIR_CONV}/log
cd ${WORKDIR}

RUNTMP=`printf %06d $RUN`
SEEDTMP=$((RUN+10000))
date

# crmc (CRMC_DIR is defined in setup_crmc_icrr.sh)
CRMC_BIN="${CRMC_DIR}/bin/crmc"
CRMC_PARAM="${LHCF_SIM_BASE}/CRMC/src/crmc/crmc_lhcf_icrr.param"
CRMC_OUTFILE="crmc_${MODEL}_${RUNTMP}.hepmc.gz"
CRMC_OUT="${WORKDIR}/${CRMC_OUTFILE}"
#cd ${CRMC_DIR}
#${CRMC_BIN} -o hepmcgz -i2212 -I2212 -p6800 -P-6800 -n100000 -s ${RUN} -c ${CRMC_PARAM} ${MODELOP} -f${CRMC_OUT} 
${CRMC_BIN} -o hepmcgz -i2212 -I2212 -p6800 -P-6800 -n500 -s ${SEEDTMP} -c ${CRMC_PARAM} ${MODELOP} -f${CRMC_OUT} 

# move the output files
if [ -f "$OUTDIR_CRMC/$CRMC_OUTFILE" ];then
    mv "$OUTDIR_CRMC/$CRMC_OUTFILE" $OUTDIR_OLD ## remove old output files.
fi
cp $CRMC_OUT $OUTDIR_CRMC

# conversion from hepmc.gz to gencol format 
CONV_DIR="${LHCF_SIM_BASE}/CRMC/src/conversion/"
CONV_BIN="${CONV_DIR}/bin/HepMCtoGencol"
CONV_OUTFILE="gen_${MODEL}_${RUNTMP}.out"
CONV_OUT="$OUTDIR_CONV/$CONV_OUTFILE"
LOG_FILE="${OUTDIR_CONV}/log/log_hepmc2gencol_${RUNTMP}.log"
QGSJET2_FLAG=""
#cd ${DIR_CONVERSION}
${CONV_BIN} -i $CRMC_OUT -o $CONV_OUT  -c 145 -b ${QGSJET2_FLAG} >& ${LOG_FILE}
 
# gzip the output file
gzip $CONV_OUT

# remove the directory 
rm -rf $WORKDIR

date

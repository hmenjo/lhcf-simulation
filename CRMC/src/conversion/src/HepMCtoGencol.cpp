/*
 * HepMC to Gencol format
 * Original code: provided from Alessio at the beginning of 2018 Apr.
 * Related codes:
 *         icrr   /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/src/Convert/
 *         Nagoya
 * /mnt/lhcfs2/data1/ohashi/Workspace/LHCf/Analysis/LHC/pp13TeV/DoubleArmSimulation/decay_correction
 * History :
 *         2022 Nov. 14th : Copied from Alessio's original code and modified for HepMC format input.
 *         2024 Mar.  1st : Copied from Ohashi's code originated Alessio's code.
 */

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
// #include <boost/program_options.hpp>

#include "HepMC/GenCrossSection.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/HEPEVT_Wrapper.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/Units.h"

// #include "IsGoodEvent.h"
// #include "testHepMCMethods.h"

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TMath.h>
#include <TROOT.h>
#include <TTree.h>

#include "ParticleCode.h"
#include "analysis.h"  // important: to read hepmc.gz file
#include "constants.h"
#include "functions.h"

using namespace std;

// #define NEUTRON

string inputfile = "";
string outputfile = "";
string output_dir = "";
string output_fileformat = "";
string debugfile = "";
int run_number = -1;
double cross_angle = 145.E-6;  // rad
bool is_zero_crossing_angle = false;
bool is_boost = true;
bool is_remove_ostapchenko = false;
bool debug = false;
double beta_y = 0.;
double gamma_y = 1.;
double shift_y = 0.;  // mm

void HandleInputPar(int argc, char **argv) {
  stringstream usage;
  usage.clear();
  usage << endl;
  usage << "Usage:" << endl << endl;
  usage << "-i\t\t\tfilename" << endl;
  usage << "-o\t\t\toutput file path (use this if you want to have only one output file)" << endl;
  usage << "-O\t\t\toutput directory" << endl;
  usage << "-F\t\t\tFormat of output filename (default= gen.%%06d.out)" << endl;
  usage << "-c\t\t\tbeam crossing angle [urad] (default = 145)" << endl;
  usage << "-b\t\t\tapply crossing angle by boost instead of simple rotation" << endl;
  usage << "-p\t\t\tremove pT=0 GeV/c events" << endl;
  usage << "-d\t\t\tdebug (provide debug filename like '-d debug001.root')" << endl;
  usage << "-h\t\t\thelp" << endl;

  const char *aux = "";

  int c;

  while ((c = getopt(argc, argv, "i:o:O:F:c:d:bph")) != -1) {
    switch (c) {
      case 'i':
        inputfile = optarg;
        if (inputfile == "") {
          cout << "Error: Please enter a valid input file name. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'o':
        outputfile = optarg;
        if (outputfile == "") {
          cout << "Error: Please enter a valid output file name. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'O':
        output_dir = optarg;
        if (output_dir == "") {
          cout << "Error: Please enter a valid output file name. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'F':
        output_fileformat = optarg;
        if (output_fileformat == "") {
          cout << "Error: Please enter a valid output file name. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'c':
        cross_angle = atof(optarg) * TMath::Power(10., -6.);  // rad
        if (cross_angle < 0.) {
          cout << "Error: Crossing angle must be positive. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'b':
        is_boost = true;
        break;
      case 'p':
        is_remove_ostapchenko = true;
        break;
      case 'd':
        debug = true;
        debugfile = optarg;
        if (debugfile == "") {
          cout << "Error: Please enter a valid debug output file name. Exit...\n";
          exit(EXIT_FAILURE);
        }
        break;
      case 'h':
        cout << usage.str().c_str() << endl;
        exit(0);
        break;
      default:
        cout << usage.str().c_str() << endl;
        exit(0);
        break;
    }
  }

  if (inputfile == "" | (outputfile == "" && output_dir == "")) {
    cout << usage.str().c_str() << endl;
    exit(0);
  }

  if (TMath::Abs(cross_angle) < 1.E-6)
    is_zero_crossing_angle = true;
  else
    is_zero_crossing_angle = false;
}

void PrintFields() {
  printf("\n");

  printf("========================================================================\n");
  printf("======================================= HepMCtoGencol_ROOT =============\n");
  printf("========================================================================\n");
  printf("============================================= Command-line Options =====\n");
  printf("========================================================================\n");
  if (is_zero_crossing_angle == true)
    printf("============================================== Zero Crossing Angle =====\n");
  else {
    printf("==================================== Crossing Angle = %3.3f urad =====\n",
           cross_angle * TMath::Power(10., 6.));
    if (is_boost)
      printf("================================== Crossing Angle Applied by Boost =====\n");
    else
      printf("=============================== Crossing Angle Applied by Rotation =====\n");
  }
  if (is_remove_ostapchenko)
    printf("======================================= Remove pT = 0 GeV/c events =====\n");
  if (debug) printf("==================================================== Debug Flag On =====\n");
  printf("========================================================================\n");
  printf("================================================== Parameters used =====\n");
  if (!is_zero_crossing_angle) {
    shift_y = -1000. * iptotan * TMath::Tan(cross_angle);  // mm

    if (is_boost) {
      beta_y = (beam_momentum / beam_energy) * TMath::Sin(cross_angle);
      gamma_y = TMath::Sqrt(1. / (1. - TMath::Power(beta_y, 2.)));

      printf("========================================================================\n");
      printf("====================================================== Apply Boost =====\n");
      printf("=============================================== Beta = %3.9f =====\n", beta_y);
      printf("============================================== Gamma = %3.9f =====\n", gamma_y);
      printf("======================================= Shift along y = %3.3f mm =====\n", shift_y);
      printf("========================================================================\n");
    } else {
      cross_angle = cross_angle;

      printf("========================================================================\n");
      printf("=================================================== Apply Rotation =====\n");
      printf("==================================== Crossing Angle = %3.3f urad =====\n",
             cross_angle * TMath::Power(10., 6.));
      printf("======================================= Shift along y = %3.3f mm =====\n", shift_y);
      printf("========================================================================\n");
    }
  }

  printf("\n");
}

int main(int argc, char **argv) {
  HandleInputPar(argc, argv);
  PrintFields();

  /* Input */
  vector<string> input_file_name;
  input_file_name.push_back(inputfile);
  DataManager data;
  data.SetFiles(input_file_name);
  Int_t NEventPerFile = 1000;
  Int_t maxNFile = 100;
  Int_t maxN = 10000;

  // /* Input */
  // // const char *input_file_name = Form("%s/%06d.root", input_dir, run_number);
  // char input_file_name[256];
  // sprintf(input_file_name, "%s/%06d.root", input_dir, run_number);
  // printf("\n");
  // printf("<<<<< Input File %s>>>>>\n", input_file_name);
  // printf("\n");
  // TFile *input = new TFile(input_file_name, "READ");
  // TTree * tree = (TTree *)input->Get("Particle");
  // Int_t NEventPerFile = 1000;
  // Int_t maxNFile = 100;
  // Int_t maxN = 10000;
  // Int_t np;
  // Double_t imp;
  // Int_t *pdgid = (Int_t *)malloc(maxN * sizeof(Int_t));
  // Int_t *ist = (Int_t *)malloc(maxN * sizeof(Int_t));
  // Double_t *p_x = (Double_t *)malloc(maxN * sizeof(Double_t));
  // Double_t *p_y = (Double_t *)malloc(maxN * sizeof(Double_t));
  // Double_t *p_z = (Double_t *)malloc(maxN * sizeof(Double_t));
  // Double_t *E = (Double_t *)malloc(maxN * sizeof(Double_t));
  // Double_t *m = (Double_t *)malloc(maxN * sizeof(Double_t));
  // tree->SetBranchAddress("nPart", &np);
  // tree->SetBranchAddress("ImpactParameter", &imp);
  // tree->SetBranchAddress("pdgid", pdgid);
  // tree->SetBranchAddress("status", ist);
  // tree->SetBranchAddress("px", p_x);
  // tree->SetBranchAddress("py", p_y);
  // tree->SetBranchAddress("pz", p_z);
  // tree->SetBranchAddress("E", E);
  // tree->SetBranchAddress("m", m);
  // const int entries = tree->GetEntries();
  // if(entries>NEventPerFile*maxNFile)
  //   {
  //     perror("Error! Too many events in input File: Now Exit...");
  //     exit(EXIT_FAILURE);
  //   }

  /* Debug */
  TFile *file_for_debug;

  TH2D *h_true_vertex_all[narm];
  TH2D *h_true_eflow_all[narm];
  TH1D *h_de_deta[narm];
  TH2D *h_de_deta_vs_phi[narm];
  TH1D *h_dsigma_deta[narm];
  TH2D *h_dsigma_deta_vs_phi[narm];
  TH1D *h_true_momentum[narm][nxyz];
  TH1D *h_true_energy[narm][nfvbin];

  TH2D *h_neutral_true_vertex_all[narm];
  TH2D *h_neutral_true_eflow_all[narm];
  TH1D *h_neutral_de_deta[narm];
  TH2D *h_neutral_de_deta_vs_phi[narm];
  TH1D *h_neutral_dsigma_deta[narm];
  TH2D *h_neutral_dsigma_deta_vs_phi[narm];
  TH1D *h_neutral_true_momentum[narm][nxyz];
  TH1D *h_neutral_true_energy[narm][nfvbin];

  const int npid = 3;
  TH1D *henergy_pid[narm][nfvbin][npid];

  if (debug) {
    file_for_debug = new TFile(debugfile.c_str(), "RECREATE");  // ROOT histograms

    const int nbinx = 260;
    const double xmin = -130;
    const double xmax = 130.;

    std::vector<double> binning_true[nfvbin];
    GetBinning(binning_true, true);

    const int nbinp = 500;
    const double pmin = -2.5;
    const double pmax = +2.5;

    file_for_debug->cd();

    // All particles
    for (int iarm = 0; iarm < narm; ++iarm) {
      h_true_vertex_all[iarm] =
          new TH2D(Form("true_vertex_all_%d", iarm + 1), "", nbinx, xmin, xmax, nbinx, xmin, xmax);
      h_true_eflow_all[iarm] =
          new TH2D(Form("true_eflow_all_%d", iarm + 1), "", nbinx, xmin, xmax, nbinx, xmin, xmax);

      h_de_deta[iarm] = new TH1D(Form("de_over_deta_%d", iarm + 1), "", 25000, 0., 25.);
      h_de_deta_vs_phi[iarm] =
          new TH2D(Form("de_over_deta_all_%d", iarm + 1), "", 25000, 0., 25., 36, 0., 360.);

      h_dsigma_deta[iarm] = new TH1D(Form("dsigma_over_deta_%d", iarm + 1), "", 25000, 0., 25.);
      h_dsigma_deta_vs_phi[iarm] =
          new TH2D(Form("dsigma_over_deta_all_%d", iarm + 1), "", 25000, 0., 25., 36, 0., 360.);
    }

    for (int iarm = 0; iarm < narm; ++iarm)
      for (int ixyz = 0; ixyz < nxyz; ixyz++)
        if (ixyz == 2)
          h_true_momentum[iarm][ixyz] = new TH1D(Form("true_momentum_%d_%d", iarm + 1, ixyz), "",
                                                 binning_true[0].size() - 1, &(binning_true[0])[0]);
        else
          h_true_momentum[iarm][ixyz] =
              new TH1D(Form("true_momentum_%d_%d", iarm + 1, ixyz), "", nbinp, pmin, pmax);
    for (int iarm = 0; iarm < narm; ++iarm)
      for (int ifv = 0; ifv < nfvbin; ifv++)
        h_true_energy[iarm][ifv] = new TH1D(Form("true_energy_%d_%d", iarm + 1, ifv), "",
                                            binning_true[ifv].size() - 1, &(binning_true[ifv])[0]);

    // Only neutral
    for (int iarm = 0; iarm < narm; ++iarm) {
      h_neutral_true_vertex_all[iarm] = new TH2D(Form("neutral_true_vertex_all_%d", iarm + 1), "",
                                                 nbinx, xmin, xmax, nbinx, xmin, xmax);
      h_neutral_true_eflow_all[iarm] = new TH2D(Form("neutral_true_eflow_all_%d", iarm + 1), "",
                                                nbinx, xmin, xmax, nbinx, xmin, xmax);

      h_neutral_de_deta[iarm] =
          new TH1D(Form("neutral_de_over_deta_%d", iarm + 1), "", 25000, 0., 25.);
      h_neutral_de_deta_vs_phi[iarm] =
          new TH2D(Form("neutral_de_over_deta_all_%d", iarm + 1), "", 25000, 0., 25., 36, 0., 360.);

      h_neutral_dsigma_deta[iarm] =
          new TH1D(Form("neutral_dsigma_over_deta_%d", iarm + 1), "", 25000, 0., 25.);
      h_neutral_dsigma_deta_vs_phi[iarm] = new TH2D(
          Form("neutral_dsigma_over_deta_all_%d", iarm + 1), "", 25000, 0., 25., 36, 0., 360.);
    }

    for (int iarm = 0; iarm < narm; ++iarm)
      for (int ixyz = 0; ixyz < nxyz; ixyz++)
        if (ixyz == 2)
          h_neutral_true_momentum[iarm][ixyz] =
              new TH1D(Form("neutral_true_momentum_%d_%d", iarm + 1, ixyz), "",
                       binning_true[0].size() - 1, &(binning_true[0])[0]);
        else
          h_neutral_true_momentum[iarm][ixyz] =
              new TH1D(Form("neutral_true_momentum_%d_%d", iarm + 1, ixyz), "", nbinp, pmin, pmax);
    for (int iarm = 0; iarm < narm; ++iarm)
      for (int ifv = 0; ifv < nfvbin; ifv++)
        h_neutral_true_energy[iarm][ifv] =
            new TH1D(Form("neutral_true_energy_%d_%d", iarm + 1, ifv), "",
                     binning_true[ifv].size() - 1, &(binning_true[ifv])[0]);

    for (int iarm = 0; iarm < narm; ++iarm)
      for (int ifv = 0; ifv < nfvbin; ifv++)
        for (int ipid = 0; ipid < npid; ipid++)
          henergy_pid[iarm][ifv][ipid] =
              new TH1D(Form("true_energy_pid_%d_%d_%d", iarm + 1, ifv, ipid), "",
                       binning_true[ifv].size() - 1, &(binning_true[ifv])[0]);
  }

  /* Output */

  // int NExpFile = entries/NEventPerFile;
  // if(entries%NEventPerFile)
  //   NExpFile += 1;
  // const int Nfile = (int)TMath::Min((double)(maxNFile), (double)(NExpFile));
  const int Nfile = maxNFile;
  std::ofstream output[Nfile];

  int ifile = -1;
  int ipar = 0;

  printf("\n");
  printf("<<<<< Input File %s >>>>>\n", input_file_name[0].c_str());
  printf("\n");

  /* Event Loop */
  long int iev = 0;
  long int iuser = 0;
  while (data.GetNextEvent())
  // for (int iev = 0; iev < entries; ++iev)
  {
    // Open the output file
    if ((outputfile != "" && iev == 0) || (outputfile == "" && (iev % NEventPerFile) == 0)) {
      string output_file_name = "";
      if (outputfile != "") {
        ifile = 0;
        output_file_name = outputfile;
      } else {
        if (ifile >= 0) {
          output[ifile].close();
          // printf(" now closed\n");
        }
        if (ifile < maxNFile - 1) ++ifile;
        printf(">>> Opening Output File n°%02d of %d at Event %05d ...\n", ifile, Nfile, iev);
        output_file_name = output_dir;
        output_file_name += "/";
        output_file_name += Form(output_fileformat.c_str(), ifile);
      }
      output[ifile].open(output_file_name.c_str(), ofstream::trunc);
      //  preamble //Eugenio: adaptation to Gencol output format (you need DoubleArm pfilter
      output[ifile] << "#  mul sub KE xyz dir  user disk49" << std::endl;
      output[ifile] << "#--------------------------------" << std::endl;
    }

    HepMC::GenEvent::particle_const_iterator par = data.evt->particles_begin();
    ipar = 0;
    for (; par != data.evt->particles_end(); ++par) {
      // for (int ipart = 0; ipart < np; ++ipart)
      // 	{
      HepMC::GenParticle *p = (*par);
      int status = p->status();

      // NB: HepMC numbering scheme is HEPEVT one, i.e.
      // 4 - original beams
      // 2 - decayed or oscillated particles
      // 1 - final particles

      // NB: Asking the decay of particles having c*tau<1cm
      //- pi0s, eta0s decay
      //- k0s oscillate (i.e. there are two particles having same properties
      //		but first one has status=1, second one status=2)

      // In order to check correct value of decay c*tau we look at LambdaZero
      if (status != 1)                           // not final particles
        if (status == 2 && p->pdg_id() == 3122)  // LambdaZero
        {
          cout << "Wrong Decay c*tau at event " << ipar << " detected particle having status "
               << status << " and pdgcode " << p->pdg_id() << endl;
          //return -1; // commented to avoid unexpected stop
        } else
          continue;

      if (is_remove_ostapchenko)
        if (TMath::Abs(p->momentum().px()) == 1.E-8 && TMath::Abs(p->momentum().py()) == 1.E-8) continue;

      double ene, mass;
      double px, py, pz;
      if (is_zero_crossing_angle) {
        ene = p->momentum().e();
        mass = p->momentum().m();

        px = p->momentum().px();
        py = p->momentum().py();
        pz = p->momentum().pz();
      } else {
        if (is_boost) {
          // This approach is equivalent to a Lorentz transormation to the reference system moving
          // downward due to beam crossing angle
          ene = +(gamma_y * p->momentum().e()) - (beta_y * gamma_y * p->momentum().py());
          mass = p->momentum().m();

          px = p->momentum().px();
          py = -(beta_y * gamma_y * p->momentum().e()) + (gamma_y * p->momentum().py());
          pz = p->momentum().pz();
        } else {
          // This approach is equivalent to a simple rotation of the trajectory of produced
          // particles in the y-z plane
          ene = p->momentum().e();
          mass = p->momentum().m();

          px = p->momentum().px();
          py = p->momentum().pz() > 0. ? (p->momentum().py() * TMath::Cos(cross_angle)) -
                                             (p->momentum().pz() * TMath::Sin(cross_angle))
                                       : (p->momentum().py() * TMath::Cos(cross_angle)) +
                                             (p->momentum().pz() * TMath::Sin(cross_angle));
          pz = p->momentum().pz() > 0. ? (p->momentum().py() * TMath::Sin(cross_angle)) +
                                             (p->momentum().pz() * TMath::Cos(cross_angle))
                                       : (-p->momentum().py() * TMath::Sin(cross_angle)) +
                                             (p->momentum().pz() * TMath::Cos(cross_angle));
        }
      }

      double ptot = TMath::Sqrt(TMath::Power(px, 2.) + TMath::Power(py, 2.) + TMath::Power(pz, 2.));
      double dx = px / ptot;
      double dy = py / ptot;
      double dz = pz / ptot;
      double ekin = ene - mass;

      // printf("%d %d %d %d %f %f %f %f %f\n", ipar, status, kcode, kcharge, ekin, px, py, pz,
      // mass);

      // if (TMath::Abs(dz)>0.999 && dz>0. && ptot>0.01) // select forward particles at the
      // p-remnant side.
      if (TMath::Abs(dz) > 0.9999995)  // Eugenio: select forward particles in Arm1 or Arm2, removing
                                      // ptot cut as in DoubleArm/pfilter.
      {
        int pdg_id = p->pdg_id();
        int kcode, kscode, kcharge;
        ParticleCode(&kcode, &kscode, &kcharge, pdg_id);

        if (kcode == krare) {
          cout << "File " << ifile << " Event " << ipar - (ifile * NEventPerFile)
               << " : Skipping Particle " << pdg_id << endl;
          continue;
        }

        ++ipar;

        // output << boost::format("%3d %2d %2d %12.5e      0.0001  0.0001  0.0001 %18.15f %18.15f
        // %18.15f %5d") %kcode %kscode %kcharge %ekin %dx %dy %dz %ipar << std::endl; Eugenio:
        // adaptation to Gencol output format (you need DoubleArm/pfilter atfer this)
        iuser = iev * 100 + ipar;
        if (ipar >= 100) 
           iuser = iev * 100 + 99;

        output[ifile]
            << Form("%3d %2d %2d %14.9e      0.0001  0.0001  0.0001  %18.15f %18.15f %18.15f %14d",
                    kcode, kscode, kcharge, ekin, dx, dy, dz, iuser)
            << std::endl;

        if (debug) {
          // Minus in x is needed to convert from downstream (COSMOS) to upstream (ANALYSIS)
          // detector reference system

          // cout << "*** Debug *** get momentum" << endl;
          double m = 1000. * iptotan / (TMath::Abs(pz / ptot));
          double x = m * (-px / ptot);
          // double y   = m * (py/ptot);
          double y = m * (py / ptot) - shift_y;
          int arm = pz > 0. ? 0 : 1;

          // cout << "*** Debug *** get radius" << endl;
          double r = GetR(-x, y);
          if (r < 150.) {
#ifdef NEUTRON
            if (kcode == 6 && ekin > Enethre_CRMC)  // Only nucleons above 500 GeV
#else
            // cout << "*** Debug *** check if photon" << endl;
            // if (in(kcode, hadronrange) && ekin>Enethre_CRMC) // All hadrons above 500 GeV
            if (in(kcode, photonrange) &&
                ekin > Enethre_CRMC)  // All photons/electrons above 200 GeV
#endif
            {
              // cout << "*** Debug *** get eta/phi" << endl;
              const double eta = GetPseudorapidity(-x, y);
              const double phi = GetPhi(-x, y);
              // cout << "*** Debug *** get rap. region" << endl;
              const int rap = ApplyRapidityReduction(-x, y);

              // cout << "*** Debug *** fill histograms" << endl;
              // All particles
              h_true_vertex_all[arm]->Fill(-x, y);
              h_true_eflow_all[arm]->Fill(-x, y, ekin);

              h_de_deta[arm]->Fill(eta, ekin);
              h_dsigma_deta[arm]->Fill(eta);
              h_de_deta_vs_phi[arm]->Fill(eta, phi, ekin);
              h_dsigma_deta_vs_phi[arm]->Fill(eta, phi);

              h_true_momentum[arm][0]->Fill(px);
              h_true_momentum[arm][1]->Fill(py);
              h_true_momentum[arm][2]->Fill(TMath::Abs(pz));

              if (rap >= 0) h_true_energy[arm][rap]->Fill(ekin);

              if (kcharge == 0) {
                // All neutral particles
                h_neutral_true_vertex_all[arm]->Fill(-x, y);
                h_neutral_true_eflow_all[arm]->Fill(-x, y, ekin);

                h_neutral_de_deta[arm]->Fill(eta, ekin);
                h_neutral_dsigma_deta[arm]->Fill(eta);
                h_neutral_de_deta_vs_phi[arm]->Fill(eta, phi, ekin);
                h_neutral_dsigma_deta_vs_phi[arm]->Fill(eta, phi);

                h_neutral_true_momentum[arm][0]->Fill(px);
                h_neutral_true_momentum[arm][1]->Fill(py);
                h_neutral_true_momentum[arm][2]->Fill(TMath::Abs(pz));

                if (rap >= 0) h_neutral_true_energy[arm][rap]->Fill(ekin);
              }

              if (rap >= 0)
              // if(kcharge==0)
              {
                int pid;

                if (kcode == 1)  // photon
                  pid = 0;
                else if (kcode == 2)  // electron
                  pid = 1;
                // if(kcode==5) //kaon
                // 	pid=0;
                // else if(kcode==6) //neutron
                // 	pid=1;
                // else if(kcode==18) //lambda0
                // 	pid=2;
                else  // everything else
                  pid = 2;

                henergy_pid[arm][rap][pid]->Fill(ekin);
              }

            }  // end of photon/electron selection
          }    // end of r < 150 mm cut
        }      // end of debug
      }        // end of forward particles selection
    }          // end of particle loop

    // printf("\n");
    output[ifile] << "" << std::endl;
    ++iev;
  }
  output[ifile].close();
  // printf(" now closed\n");

  if (debug) {
    file_for_debug->Write();
    file_for_debug->Close();
  }

  return 0;
}

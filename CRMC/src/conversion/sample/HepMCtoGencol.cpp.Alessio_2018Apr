#include <iostream>
#include <fstream>

#include "HepMC/GenEvent.h"
#include "HepMC/GenCrossSection.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/IO_AsciiParticles.h"

#include "IsGoodEvent.h"
#include "testHepMCMethods.h"

#include <boost/format.hpp>
#include <boost/program_options.hpp>

#include <TMath.h>

#include "ParticleCode.h"

using boost::program_options::options_description;
using boost::program_options::value;
using boost::program_options::variables_map;
using boost::program_options::store;
using boost::program_options::parse_command_line;
using boost::program_options::notify;
using boost::io::str;

int main(int argc, char** argv)
{ 
  /* Command line option */
  options_description opt("Option");
  // define format
  opt.add_options()
    ("help,h", "display help")
    ("index,i", value<int>(), "file index");
  
  // analyze command line
  variables_map argmap;
  store(parse_command_line(argc, argv, opt), argmap);
  notify(argmap);
  
  // if no matching option, show help
  if (argmap.count("help") || !argmap.count("index"))
  {
    std::cerr << opt << std::endl;
    return 1;
  }
  
  // copy commands to variables
  int index = argmap["index"].as<int> ();

  /* Output */
  std::ofstream ofs;
  ofs.open((str(boost::format("%05d_test.out") %index)).c_str());

  /* Input */
  HepMC::IO_GenEvent ascii_in((str(boost::format("%05d.out") %index)).c_str(), std::ios::in);
  ascii_in.use_input_units( HepMC::Units::GEV, HepMC::Units::MM );	

  HepMC::GenEvent* evt = ascii_in.read_next_event();

  int ipar = 0;

  // preamble
  ofs << "#  mul sub KE xyz dir  user disk49" << std::endl;
  ofs << "#--------------------------------" << std::endl;  

  while (evt)
  {
    ipar = 0;

    for (HepMC::GenEvent::particle_const_iterator p 
	   = evt->particles_begin(); p != evt->particles_end(); ++p )
    {
      if (! (*p)->is_beam())
      {
	double px   = (*p)->momentum().x();
	double py   = (*p)->momentum().y();
	double pz   = (*p)->momentum().z();
	double ene  = (*p)->momentum().e();
	double mass = (*p)->generated_mass();

	double ptot = TMath::Sqrt(TMath::Power(px, 2.) + TMath::Power(py, 2.) + TMath::Power(pz, 2.));
	double dx   = px/ptot;
	double dy   = py/ptot;
	double dz   = pz/ptot;
	double ekin = ene - mass;
	
	ipar++;
	if (TMath::Abs(dz)>0.999 && dz>0. && ptot>0.01) // select forward particles at the p-remnant side.
	  {
	    int pdg_id  = (*p)->pdg_id();
	    int kcode, kscode, kcharge;
	    ParticleCode(&kcode, &kscode, &kcharge, pdg_id);
	    
	    ofs << boost::format("%3d %2d %2d %12.5e      0.0001  0.0001  0.0001 %18.15f %18.15f %18.15f %5d")
	      %kcode %kscode %kcharge %ekin %dx %dy %dz %ipar << std::endl;
	  }
      }
    }
    ofs << "" << std::endl;

    // clean up and get next event
    delete evt;
    ascii_in >> evt;
  }

  ofs.close();
  return 0;
}

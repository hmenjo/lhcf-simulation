#ifndef PARTICLECODE_H_
#define PARTICLECODE_H_

#include <iostream>
#include <cassert>
#include <TMath.h>
#include <TRandom3.h>

#include "PDGcode.h"
#include "COSMOScode.h"

void ParticleCode(int *code, int *subcode, int *charge, const int &pdg)
{
  const int pdgabs = TMath::Abs(pdg);

  if (pdgabs==kfpion)
    {
      *code    = kpion;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1,  pdg);
    }
  else if (pdgabs==kfpi0)
    {
      *code    = kpion;
      *subcode = 0;
      *charge  = 0;
    }
  else if (pdgabs==kfkaon)
    {
      *code    = kkaon;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1,  pdg);
    }
  else if (pdgabs== kfk0l)
    {
      *code    = kkaon;
      *subcode = TMath::Sign(k0l, pdg);
      *charge  = 0;
    }      
  else if (pdgabs== kfk0s)
    {
      *code    = kkaon;
      *subcode = TMath::Sign(k0s, pdg);
      *charge  = 0;
    }
  else if (pdgabs== kfk0)
    {
	  TRandom3 r(0);
	  int type;
	  if(r.Rndm()<=0.5)
		  type=k0s;
	  else
		  type=k0l;

      *code    = kkaon;
      *subcode = type;
      *charge  = 0;
    }
  else if (pdgabs==kfneutron)
    {
      *code = knuc;
      if (pdg>0)
	*subcode = kneutron;
      else
	*subcode = kneutronb;
      *charge = 0;
    }
  else if (pdgabs==kfproton)
    {
      *code = knuc;
      if(pdg>0)
	*subcode = regptcl;
      else
	*subcode = antip;
      *charge = TMath::Sign(1, pdg);
    }
  else if (pdgabs==kfeta)
    {
      *code    = keta;
      *subcode = 0;
      *charge  = 0;
    }
  else if (pdgabs==kfelec)
    {
      *code    = kelec;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1, -pdg);
    }
  else if (pdgabs==kfmuon)
    {
      *code    = kmuon;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1, -pdg);
    }
  else if (pdgabs==kfneue)
    {
      *code    = kneue;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    }	
  else if (pdgabs==kfneumu)
    {
      *code    = kneumu;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    }
  else if (pdgabs==kfphoton)
    {
      *code    = kphoton;
      *subcode = 0;
      *charge  = 0;
    }
  else if (pdgabs==kfdmes)
    {
      *code    = kdmes;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1,  pdg);
    }
  else if (pdgabs==kfd0)
    {
      *code    = kdmes;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    }
  else if (pdgabs==kflambda)
    {
      *code    = klambda;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    }
  else if (pdgabs==kfsigma0)
    {
      *code    = ksigma;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    }
  else if (pdgabs==kfsigmap)
    {
      *code     = ksigma;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1,  pdg);
    }      
  else if (pdgabs==kfsigmam)
    {
      *code    = ksigma;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1, -pdg);
    }
  else if (pdgabs==kfgzai0 )
    {
      *code    = kgzai;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = 0;
    } 
  else if (pdgabs==kfgzai)
    {
      *code    = kgzai;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1, -pdg);
    }
  else if (pdgabs==kflambdac)
    {
      *code    = klambdac;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1,  pdg);
    }
  else if (pdgabs==kfbomega)
    {
      *code    = kbomega;
      *subcode = TMath::Sign(1, -pdg);
      *charge  = TMath::Sign(1, -pdg);
    }
  else if (pdgabs==kfrho)
    {
      *code    = krho;
      *subcode = 0;
      *charge  = 0;
    }
  else if (pdgabs==kfmomega)
    {
      *code    = komega;
      *subcode = 0;
      *charge  = 0;
    }
  else if (pdgabs==kfphi)
    {
      *code    = kphi;
      *subcode = 0;
      *charge  = 0;
    }
  else
    {
      //std::cout << "unknown pdg-id (" << pdgabs << "): code = krare, subcode = pdg" << std::endl;
      *code = krare;
      *subcode = 0;
      *charge  = 0;
      //assert(!"Error : No corresponding particle is found.");
    }
}

#endif // PARTICLECODE_H_

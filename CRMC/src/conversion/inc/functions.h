#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include <TMath.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TH1D.h>
#include <TGraphAsymmErrors.h>

using namespace std;

//==========================================================
//======= Functions related position selection =============
//==========================================================

double GetR(double x, double y)
{
  const double r = TMath::Sqrt(TMath::Power(x, 2.) + TMath::Power(y, 2.));

  return r;
}

double GetTheta(double x, double y)
{
  const double theta = TMath::ATan(GetR(x, y)/(1000.*iptolhcf));

  return theta;
}

double GetPseudorapidity(double x, double y)
{
  const double eta = -TMath::Log(TMath::Tan(GetTheta(x, y)/2.));

  return eta;
}

double GetPhi(double x, double y)
{
  //From upstream
  //phi starts from x axis and is positive counterclockwise from 0 to 360°
  double phi = TMath::ATan2(y, x)/TMath::Pi()*180.;
  if(phi<0.) phi += 360.;

  return phi;
}

int ApplyRapidityReduction(double r)
{
  for (int ifv = 0; ifv < nfvbin; ++ifv)
    if(r >= r_interval[ifv][0] && r < r_interval[ifv][1])
      return ifv;

  return -1;
}

int ApplyRapidityReduction(double x, double y)
{
  const double r = GetR(x, y);

  return ApplyRapidityReduction(r);
}

bool in(int var, const int interval[2])
{
  if(var >= interval[0] && var <= interval[1])
    return true;
  else
    return false;
}

bool out(int var, const int interval[2])
{
  if(var >= interval[0] && var <= interval[1])
    return false;
  else
    return true;
}

//==========================================================
//======= Functions to copy binning in std::vector =========
//==========================================================

void GetBinning(std::vector<double> binning[nfvbin], bool true_energy=false)
{
  /* TFile *file; */
  /* if(true_energy) */
  /*   file = new TFile(bin_true_file.c_str(), "read"); */
  /* else */
  /*   file = new TFile(bin_file.c_str(), "read"); */
  /* TTree *tree = (TTree*)file->Get("Bin"); */

  /* std::vector<double>     *vbin[nfvbin]; */
  /* TBranch *bbin[nfvbin]; */
  /* for(int irap=0; irap<nfvbin; ++irap) */
  /*   { */
  /*     vbin[irap]  = new vector<double>(); */
  /*     bbin[irap] = tree->GetBranch(Form("binning_%d", irap)); */
  /*     tree->SetBranchAddress(Form("binning_%d", irap), &vbin[irap], &bbin[irap]); */
  /*   } */

  /* const int nentries = tree->GetEntries(); */
  /* if(nentries != 1) */
  /*   { */
  /*     cout << "Strange number of entries in binning file: Exit..." << endl; */
  /*     exit(EXIT_FAILURE); */
  /*   } */

  /* tree->GetEntry(0); */
  /* for(int irap=0; irap<nfvbin; ++irap) */
  /*   binning[irap] = *vbin[irap]; */
  /* file->Close(); */

  /* cout << "GetBinning: begin" << endl; */

  if (true_energy)
    {
      for (int ifv = 0; ifv < nfvbin; ++ifv)
	for (int ie = 0; ie <= 70; ++ie) binning[ifv].push_back((double)ie *  100.);
    }
  else
    {
      for (int ifv = 0; ifv < nfvbin; ++ifv)
	{
	  /* cout << "ifv = " << ifv << endl; */
	  if (ifv <= 2)
	    {
	      // small tower
	      for (int ie = 0; ie <= 20; ++ie) binning[ifv].push_back(        (double)ie *  100.);
	      for (int ie = 1; ie <= 10; ++ie) binning[ifv].push_back(2000. + (double)ie *  200.);
	      for (int ie = 1; ie <=  2; ++ie) binning[ifv].push_back(4000. + (double)ie *  500.);
	      for (int ie = 1; ie <=  2; ++ie) binning[ifv].push_back(5000. + (double)ie * 1000.);
	    }
	  else
	    {
	      // large tower
	      for (int ie = 0; ie <= 20; ++ie) binning[ifv].push_back(        (double)ie *  100.);
	      for (int ie = 1; ie <=  5; ++ie) binning[ifv].push_back(2000. + (double)ie *  200.);
	      for (int ie = 1; ie <=  2; ++ie) binning[ifv].push_back(3000. + (double)ie *  500.);
	      for (int ie = 1; ie <=  3; ++ie) binning[ifv].push_back(4000. + (double)ie * 1000.);
	    }
	}
    }
  /* cout << "GetBinning: done" << endl; */
  
  return;
}

#endif /*FUNCTIONS_H_*/

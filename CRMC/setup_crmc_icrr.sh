# For CRMC MC generation

# module load python/3.8.3
module load cmake/3.17.3

#source /usr/local/gcc540/ROOT/6.08.06/bin/thisroot.sh
#export CC=/usr/local/GNU/5.4.0/bin/gcc
#export CXX=/usr/local/GNU/5.4.0/bin/g++

export BOOST_ROOT=/disk/lhcf/user/simulation_Run3/local/
export HEPMC_ROOT=/disk/lhcf/user/simulation_Run3/local/
export HEPMC3_ROOT=/disk/lhcf/user/simulation_Run3/local/
export PYTHON3_ROOT=/disk/lhcf/user/simulation_Run3/local/
export LD_LIBRARY_PATH=$BOOST_ROOT/lib:$HEPMC3_ROOT/lib64:$PYTHON3_ROOT/lib:$LD_LIBRARY_PATH
export PATH=/disk/lhcf/user/simulation_Run3/local/bin/:$PYTHON3_ROOT/bin:$PATH

export LHCF_SIM_BASE=/disk/lhcf/user/simulation_Run3/lhcf-simulation/
export CRMC_DIR=/disk/lhcf/user/simulation_Run3/local/crmc-v1.8.0/

source /disk/lhcf/user/simulation_Run3/local/root6/bin/thisroot.sh


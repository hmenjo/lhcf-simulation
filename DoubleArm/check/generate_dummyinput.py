'''
Generate dummy Double input file for checking beam center position 
'''

import math

if __name__ == "__main__":
    
    #filename = 'dummy_input_photon500GeV.out'
    filename = 'dummy_input_neutron1TeV.out'
    with open(filename, mode='w') as f:
    
        header = '#  mul sub KE xyz dir  user disk49\n#--------------------------------\n'
        f.write(header)
        
        crossing_angle = 145.E-6
        n = 100
        for i in range(0,n):
            #line_p = '   1  0  0    500.000    0.001  0.001  0.001'  # photon 500 GeV
            line_p = '   6 -1  0   1000.000    0.001  0.001  0.001'  # neutron 1 TeV
            line_w = ' {:19.15f} {:19.15f} {:19.15f}'.format(0., -1.*math.sin(crossing_angle),math.cos(crossing_angle))
            line = line_p + line_w + ' {:10d}\n'.format(i*100+1)
            f.write(line)
            line_w = ' {:19.15f} {:19.15f} {:19.15f}'.format(0., -1.*math.sin(crossing_angle),-1.*math.cos(crossing_angle))
            line = line_p + line_w + ' {:10d}\n'.format(i*100+2)
            line += '\n'
            f.write(line)
    
            
    
include  ${EPICSTOP}/site.config

INCHEADER  +=  -I${EPICSTOP}/UserHook/

objs =  ephookLHC.o  eppos2B.o

sepicsLHC$(ARCH): $(objs)
	$(LD) $(LDFLAGS) -o $@ $(objs) -v  -L$(DEST) -l$(LIBNAME) -L$(COSMOSTOP)/lib/$(ARCH) -lcosmos

clean:;		@rm -f $(OBJS) core *~ a.out


ephookLHC.o: ${EPICSINC}/Zepdef.h \
	${EPICSINC}/Zuser.h \
	${COSMOSINC}/BlockData/cblkTracking.h \
	./epUI.f


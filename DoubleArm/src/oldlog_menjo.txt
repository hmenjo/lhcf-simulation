**************    DoubleArm REAMDE    **************

++ 060316 ++

以前からちまちまとやってはいたが本格的に、
磁場とパイプいれたシミュレーションを行なうことにする。

とはいうもののほとんどが笠原先生が作ったDoubleArmという
プログラムを流用させてもらう。

DoubleArmのありかは

~kasahara/Epics/UserHook/LHC/DoubleArm

となっている。

注意点として以下がある。
・新しい物質を導入した場合にfordpmjetを実行して、あらたに
  dpmjet.GLBとdpmjet.inpを作成する。このときに、originalそのままだと
  うまく実行できない。
  configdarm内を
  2 arm  0 0 0 / 0 0 +
  3 arm  0 0 0 / 0 0 -0.05  -1 0 0  0 1 0
   -> 2 arm  sp 0 0 0 / 0 0 +
      3 arm  sp 0 0 0 / 0 0 -0.05  -1 0 0  0 1 0
  と変更すればOK
・putcasflag.f は粒子が一度でもパイプ内にはいったかどうかを識別するためのもの
  この中身では、cTrack等の定義が見当たらないがincludeされているZepTrackv.h
  内にこれらの定義がある。これをつかってデータの受渡しが行なわれているらしい。
  旧versionのEpics Gencolでは出力にuserの項目がないので、旧データを
  使用して計算する場合にはBKG粒子のtagは0のままになってしまう。


いよいよ実際に改良を行なっていく。

まずはBeamHaloによる影響の計算。
BeamHaloで一番いま影響がでると考えられるのがIPから20m地点にあるTAS。

TASにぶちあたるprotonからの影響が一番心配されている。
そこで今あるconfigdarmにTASの構造をいれて、そこにprotonをぶちあてるような
計算を行なうことにする。

TASの構造は、
inner radius (Aperture): 17mm
length                 : 1.8m  11.96λi ,125.9 X0
material               : cupper
Position (TAS surface) : 19.45 m (論文によっては19.05m)

これを加えたものをconfigdarm_mとする。
TASより内側やTAS直後のQ-Magnetの構造はいれない。
D1マグネットまでは内径2.65mのパイプがあるものとする。

次に、ephookLHC.fをTASに衝突する粒子がR^-4[/cm2]の分布に従うように
入射粒子位置を乱数で決定できるように変更。
-> ephookLHC_beamhalo.f 

いきなり分布にしたがったものを計算するよりは、一点で計算してみる。
x,y = (1.72,0.) に7TeV 粒子x100events 
-> BeamHalo_Positionfix_001_060315.out

次に ephookLHC_beamhalo.f に変更してR^-4の場合を計算する。
入射粒子は 1.7 < R < 2.0
-> BeamHalo_R4_001_060315.out

++ 060318 ++
統計をふやすため再度実行
-> BeamHalo_R4_002_060318.out

++ 060319 ++
BeamHalo_R4_002_060318.out を途中でとめて
TASへのビーム入射位置が出力されるように改良。
-> BeamHalo_R4_003_060319.out

++ 060320 ++
BeamHalo_R4_003_060319.out を800eventsくらいでストップ

ここまでのBeamHaloについてはここまでにして
これからはBeamGasについてのSimulationを行なう。

BeamGasについては以前、p-H衝突の2次粒子の分布を作成して考察を行なった。
今回は、実際にPipeで衝突を起こしてDetectorで検出するまでを行なう。

これによりBeam-GasによるPi0再構成への洩れ込みを考察する。

プログラム自体は今までのものを流用して、
これに+primaryで読みこんだ衝突がどこでおこったかを乱数をつかって
計算できるようにする。(これがやっかいだった)

7TeV陽子の軌道を近似的に算出するサブルーチンをputorbit.fに作成した。
これはz方向の位置を与えれば、x,y,theta(軌道の曲がり具合)を計算するもの。

あとは磁場がオリジナルではI.P.の右左で同一極性になっていたのでこれを反転。
X形に交差するように改良した。

-> 軌道近似および磁場はOK。ほぼどの位置からproton@7TeVを入射しても
   TAN前面で7cm台にくる。(正確な値は記録しわすれた)

出力は、今までのIntaraction解析プログラムを流用できるように同じようにした。
ただし、衝突点情報として
PGAS-COL   x  y  z  theta
を出力してある。

ここまでの注意点をあげておく
・config(configdarm_nom)はconfigdarm_mとともにオリジナルからかえてある。
  笠原先生がマグネットユニットを5個として作っていたので、それを6個に
  変更したのと、マグネットのパイプ等のならびをわかりやすくした。
・ephookLHC.f内 uafi1ev で入射粒子の情報の書き換えを行なう際だが、
  aTrack内の位置情報などはこの時点では、Local Componentの座標で記述されて
  いることに注意。そのため、最終的にはcall eppos2cn(ncx,aTrack,nc)で
  World座標からComponentを受け取って、Local 座標で与えないといけない。 
  call eppos2cn(ncx,aTrack,nc)
  call epw2ldm(aTrack.cn,ww,aTrack.w,aTrack.p)
  それぞれのサブルーチンについては、eppos2cn.f内を参照。

++ 060321 ++ 

シェルを組んでいっきに計算できるようにした。
qgsjet2 100,000 Events x 10 個
 pH_qgsjet2_100000events_060321_1.out 

++ 060414 ++
せっかく流していたtasim上の計算結果が使えないことが判明。
うっかりepicsfileを他のものといれかえてしまったせいで、
磁場を使わないような設定になってしまっていた。
ショック!!
 
++ 061213 ++
450GeV用のシミュレーションを本格的に開始する。
まずはFULLシミュレーションということで、DoubleArmの計算を行なう。

DoubleArm使い方のおさらい(思い出せる範囲で)。

DoubleArmのシェルは、runsub_pos00.shといろいろあるけど
基本は同じ。


1. Genecolにてpp衝突を計算。
   このときのパラメータファイルはDoubleArmの下部ディレクトリにあり、
   それの違いがシェルの違いになっている。
2. それをDoubleArmプログラムにてパイプを走らせる。
   DoubleArmのcomp_configdarm_mはz+-両方向がちゃんとある。
   しかし、Gencolのところで出力にカットをいれていると両方向にはならないので
   注意。

作業の流れはEnd2Endほどやっかいではない。

ここからは450GeV計算用の設定。
Epics/Util/Gencol/dpmjet.inpの編集 (重要!!)
   ENERGY          4.32e5 (4.316e5としていたらエラーが出た...??)
param -> param_dpmjet3_pos00_450GeV
   DestEventNo = 10000
   UserHookc ='6 1  1    0.  0.   450.',
              '6 1  1    0.  0.  -450.',
   UserHookr = -1.0 1.0 14000
2armのコインシデンスも計算することを考えて、-1から1にしておく。
(ただPtが大きいものが本当ならパイプ外のものによって遮られるはずが
 誤って入り込んでしまわないか不安。)

input -> input_pos00_450GeV
  'epicsfile', 'configdarm_m', 'BeamBeam/sepicsfile/sepicsfile_pos00_450GeV', f, 'param'/

sepicsfile -> sepicsfile_pos00_450GeV
  Nevent     1   10000 
  PrimaryFile  '/tmp/menjo/+primary_pos00_450GeV.out' 

磁場を弱くする。
  eppos2B.f.450GeV を eppos2B.fにコピー

ephook.f 内を少し改良して、+-両方で出力するように改良。
また、出力範囲を少し広げてxy <+-10cm , r<10cmとした。


ここからは、すこしephook.fの改良のお話。
テストとして計算させたものを見ていると、どうも-側の出力でwzが-になっていない。
もしかすると、-側は反転させて作っているからlocalとworldの方向が異なってしまうのではないか。
positionについてと同じ様に epl2w の親戚の  epl2wdを使ってみる。
(参考 Epics/prog/eppos2cn.f)

ecylが認識されなかった。
Util/にて、
./usenewvol ../../../MyEpics/LHCf/DoubleArm/source/configdarm_m

計算開始 
  10624  12/13/2006 19:03:05 tasim522  ./runsub_pos00_450GeV.sh 1 3
  10625  12/13/2006 21:50:22 tasim517  ./runsub_pos00_450GeV.sh 4 6
  10626  12/13/2006 21:54:42 tasim517  ./runsub_pos00_450GeV.sh 7 9
  10627  12/13/2006 21:59:01 tasim527  ./runsub_pos00_450GeV.sh 10 12
  10628  12/13/2006 22:00:02 tasim511  ./runsub_pos00_450GeV.sh 13 15

++ 061214 ++
tasim上でいろいろと改良を行なったので、なかみをlindaq1にコピー

tasim の CPUがあいたのでJOBをながす。
  10630  12/14/2006 14:00:11 tasim525  ./runsub_pos00_450GeV.sh 16 18
  10631  12/14/2006 14:00:26 tasim515  ./runsub_pos00_450GeV.sh 19 21
  10632  12/14/2006 14:02:29 tasim505  ./runsub_pos00_450GeV.sh 22 24
  10633  12/14/2006 14:02:44 tasim514  ./runsub_pos00_450GeV.sh 25 27
  10634  12/14/2006 14:03:00 tasim519  ./runsub_pos00_450GeV.sh 28 30
  10635  12/14/2006 14:10:06 tasim506  ./runsub_pos00_450GeV.sh 31 35
  10636  12/14/2006 14:10:06 tasim507  ./runsub_pos00_450GeV.sh 36 40
  10637  12/14/2006 14:11:36 tasim512  ./runsub_pos00_450GeV.sh 41 45
  10638  12/14/2006 14:11:36 tasim510  ./runsub_pos00_450GeV.sh 46 50



++ 061223 ++
DPMでの計算は500,000events分が終わった。
今度はQGSでの計算をこころみようと思う。

が、.shファイルのなかみをよくみてみるとprimaryファイルなど
出力ファイル名が同一のものになっており、同一マシンで２つプログラムが
実行された場合には、名前がかぶってしまって計算ミスが生じてしまう。
そこで.sh内を改良。データ番号がテンポラルファイル名にもつくようにした。

qgsjet2にて計算するようにシェルを改良
-> runsub_pos00_450GeV_qgsjet2.sh
大事なのはGENCOLはモデルによって改良コンパイルが必要だということ。

  10903  12/23/2006 21:50:42 tasim514 ./runsub_pos00_450GeV_qgsjet2.sh 1 10
  10904  12/23/2006 21:50:42 tasim505 ./runsub_pos00_450GeV_qgsjet2.sh 11 20
  10905  12/23/2006 21:50:57 tasim514 ./runsub_pos00_450GeV_qgsjet2.sh 21 30
  10906  12/23/2006 21:55:49 tasim525 ./runsub_pos00_450GeV_qgsjet2.sh 31 40
  10907  12/23/2006 21:50:42 tasim505 ./runsub_pos00_450GeV_qgsjet2.sh 41 50 

++ 080410 ++
DoubleArmを少し計算しなおす。(伊藤先生に頼まれてたもあって。)

まずはやり方を思い出す。(昔はシンボリックリンクを使ってなかったんだな〜)
今の設定は、
・ ephookLHC.f -> ephookLHC.f.450GeV 
                  すこし改良してあるんだが、本質的には一緒。
・ eppos2B.f   -> eppos2B.f.450GeV

これを7TeV用に変更したいんだが、
まず ephookLHC.f は本質的に変更が必要なものではないはずなので
そのままにしておく。 eppos2B.fはシンボリックリンクにして、
eppos2B.f.7TeV にはる。

計算しようと思ったら、ecylがないとエラーがでた。
これの対処の方法は、EpicsのREADMEに書いてあって、
Epics/Util/にて
./usenewvol "config"
とすればよい。

++ 080813 ++
ICRR計算サーバーで使えるように調整を行なっていたら、違うバグを発見。
いままでsepicsfile内の入射場所指定は、InputP 'fix' でOKだったのだが
新しいEpics Versionだとこのままだと固まってしまう。
InputP  'u+z' とすることで対処できる。






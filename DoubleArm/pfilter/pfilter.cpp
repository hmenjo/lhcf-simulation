#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "pfilecnt.h"
using namespace std;

int main(int argc, char** argv){
  int maxnevent=-1;
  int startevent=0;
  double cutwz=0; 
  bool user0=false;  // for special run
  pfilecnt con;
  con.setinputfile("gencol.out");
  con.setoutputfile("+primary");

  for(int i=0;i<argc;i++){
    if(strcmp(argv[i],"-i")==0){
      con.setinputfile(argv[++i]);
    }
    if(strcmp(argv[i],"-o")==0){
      con.setoutputfile(argv[++i]);
    }
    if(strcmp(argv[i],"-nev")==0){
      maxnevent = atoi(argv[++i]);
    }
    if(strcmp(argv[i],"-sev")==0){
      startevent = atoi(argv[++i]);
    }
    if(strcmp(argv[i],"-wz")==0){
      cutwz = atof(argv[++i]);
    }
    if(strcmp(argv[i],"-user0")==0){
      user0=true;
    }  
    if(strcmp(argv[i],"-h")==0 || 
       strcmp(argv[i],"-help")==0 || 
       strcmp(argv[i],"--help")==0){
      cout << "Primary Filter for DoubleArm ---------------------- " << endl
	   << "exp) pfilter -if gencol.out -of +primary -nev 100 -sev 10"<< endl;
      cout << "    -i  \"file\"  : input filename " << endl
	   << "    -o  \"file\"  : output filename " << endl
	   << "    -nev \"value\": number of converted events " << endl
	   << "    -sev \"value\": convertion start event number " << endl;
      cout << "    -wz  \"value\": particle selection with < value " << endl;  
      cout << "    -user0        : for special run. Don't use its option." << endl;  
      return 0;
    }
  }
  
  if( con.inputfile("") < 0){  exit(-1);}
  con.outputfile("");
  con.writeheader();
  
  int ret;
  int nevent=0;
  int ievent=0;
  while(1){
    ret = con.readparticle();
    if(ret<0) {break;}
    else if(ret>=2){ continue; } 
    else if(ret==1){
      if(ievent>=startevent){
	con.writeendevent();
	nevent++;
	if(maxnevent>0 && nevent>=maxnevent){
	  break;
	}
      }
      ievent++;
      continue;
    }
    else if(ret==0){
      if(ievent>=startevent){
	// cuts
	if( fabs(con.getparticle()->w[2]) < cutwz) { continue ;}
	
	// for specail run
	if( user0==true){
	  con.getparticle()->user = 0;
	}
	
	con.writeparticle();
      }
    }
  }
  
  cout << "==== Primary Filter for DoubleArm ==============" << endl
       << "Input:  " << con.getinputfile() << endl
       << "Output: " << con.getoutputfile() << endl
       << "Nevent: " << nevent << endl
       << "Comments: " << endl;

  return 0;
}


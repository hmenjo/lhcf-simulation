import subprocess
import time
import sys
import os
import re
import glob
from datetime import datetime, timedelta
from logging import getLogger, StreamHandler, Formatter, FileHandler, DEBUG
from socket import gethostname

# History 
#    Original file : /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/run/loop.py
#    2022 Nov. 11th: Copied from menjo-san's directory and modified. 

if sys.version_info.major == 3:
    # Python 3
    unicode = str

class JobController:
    def __init__(self) -> None:
        self.user = ''
        self.logger = getLogger(__name__)
        self.hostname = gethostname()  # icrhome10 or lhcfs2.stelab.nagoya-u.ac.jp
        self.dir_run = './run/'
        self.dir_submitlog = './submitlog/'

        # Cluster control command
        if 'icrhome' in self.hostname:
            self.jobstat = 'pjstat'
            self.jobsub = 'pjsub'
        elif 'lhcfs2' in self.hostname:
            self.jobstat = 'qstat'
            self.jobsub = 'qsub'
        else:
            print('Unkown cluster system.')
            exit()

    def set_user(self, username):
        self.user = username

    def make_directories(self):
        if not os.path.exists(self.dir_run):
            os.makedirs(self.dir_run)
        if not os.path.exists(self.dir_submitlog):
            os.makedirs(self.dir_submitlog)

    def setup_logger(self, logfile='submitlog/submitlog.txt'):
        self.logger = getLogger(__name__)
        self.logger.setLevel(DEBUG)

        sh = StreamHandler()
        sh.setLevel(DEBUG)
        formatter = Formatter('%(asctime)s: %(message)s')
        sh.setFormatter(formatter)
        self.logger.addHandler(sh)

        fh = FileHandler(logfile)  # fh = file handler
        fh.setLevel(DEBUG)
        fh_formatter = Formatter('%(asctime)s : %(message)s')
        fh.setFormatter(fh_formatter)
        self.logger.addHandler(fh)
        return self.logger

    def njob(self):
        ret = subprocess.check_output(
            '{} | grep {} | wc'.format(self.jobstat, self.user), shell=True)
        vals = ret.split()
        n = int(vals[0])
        return n

    def waitjob(self, limit=500):
        while True:
            if self.njob() <= limit:
                break
            time.sleep(10)
        return

    def submit_job (self, orgfile, run, model='EPOSLHC', tag='job'):

        with open(orgfile, 'r') as f:
            data = f.read()
        
        # Run Number    
        data = data.replace('$1','{}'.format(run))

        # Model name
        data = data.replace('EPOSLHC',model)

        print(model)
        # CRMC option for run_crmc_darm.sh
        if model=='EPOSLHC':
            data = data.replace('-m0','-m0')
        elif model == 'QGSJET2_04':
            data = data.replace('-m0','-m7')
            daat = data.replace('QGSJET2_FLAG=""', 'QGSJET2_FLAG="-p"')
        elif model == 'DPMJET3_2019':
            data = data.replace('-m0','-m12')
        elif model == 'SIBYLL2.3d':
            data = data.replace('-m0','-m6')

        filename = './{}/{}_run{}.sh'.format(self.dir_run, model,run) 
        with open(filename,mode='w') as f:
            f.write(data)

        ret = subprocess.call('{} {} '.format(self.jobsub, filename), shell=True)
        self.logger.info('submit {} ({})'.format(filename, orgfile))
        return 

    def loop_one_run (self, orgfile, run) :
        step = 50000
        for i in range(0,40):
            waitjob()
            self.submit_job(orgfile, run, i*step, (i+1)*step-1)
        return 

    def send_mail(self, n_submitted, n_total, n_days, start_time):
        # Send a daily report by email

        f = open("mail_{}.txt".format(self.user), 'r')
        data = f.read()
        f.close()

        data = data.replace('__SUBMITTED__', '{}'.format(n_submitted))
        data = data.replace('__TOTAL__', '{}'.format(n_total))
        data = data.replace('__NDAYS__', '{}'.format(n_days))
        data = data.replace('__START_DATE__', start_time.strftime('%Y/%m/%d'))

        filename = "./{}/mail_{}.txt".format(self.dir_submitlog, self.user)
        f = open(filename, 'w')
        f.write(data)
        f.close()

        ret = subprocess.call(
            'cat {} | sendmail -i -t'.format(filename), shell=True)
        return

    def loop_jobs(self, model, srun=1, erun=1000, limit=500, orgfile='run.sh'):
        # Model : EPOSLHC, QGSJET2_04, SIBYLL2.3d, DPMJET3_2019

        # loop of files
        n_submitted = 0
        n_total = erun - srun + 1

        start_time = datetime.now()
        pre_time = start_time
        for run in range(srun, erun+1):
            self.waitjob(limit)
            self.submit_job(orgfile, run, model)
            n_submitted += 1

            current_time = datetime.now()
            if current_time > pre_time + timedelta(days=1):
                self.send_mail(n_submitted, n_total,
                               (current_time - start_time).days, start_time)
                pre_time = datetime.now()


# #### Loop for specific data sets
                        
# def do_EPOS (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
#     for run in range(srun,erun+1):
#          waitjob(limit)
#          submit_job (orgfile, run, model='EPOSLHC')
#     send_mail( model='EPOSLHC')
#     return
   
# def do_QGS (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
#     for run in range(srun,erun+1):
#          waitjob(limit)
#          submit_job (orgfile, run, model='QGSJET2_04')
#     send_mail( model='QGSJET2_04')
#     return

# def do_SIBYLL (srun=1, erun=1000, limit = 500, orgfile = 'run.sh'):
#     for run in range(srun,erun+1):
#          waitjob(limit)
#          submit_job (orgfile, run, model='SIBYLL2.3d')
#     send_mail( model='SIBYLL2.3d')
#     return
   
# def do_DPM (srun=1, erun=1000., limit = 500, orgfile = 'run.sh'):
#     for run in range(srun,erun+1):
#          waitjob(limit)
#          submit_job (orgfile, run, model='DPMJET3_2019')
#     send_mail( model='DPMJET3_2019')
#     return
   
# #### main
# if __name__ == '__main__' :
#     os.makedirs('./run/', exist_ok=True)   
#     os.makedirs('./submitlog/', exist_ok=True)   
#     setup_logger(logfile="./submitlog/log.txt")
  
    
#     do_EPOS(1, 1000, limit = 500)
#     do_QGS(1, 1000, limit = 500)
    
#     # with beampipe material 
#     #do_EPOS(1, 2000, limit = 250, orgfile='run_wPipe.sh')
    
#     #do_EPOS(3001,5000)
#     #do_QGS(1,100) 
#     #do_SIBYLL(3001,5000)
#     #do_DPM(3001,5000)


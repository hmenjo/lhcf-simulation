from jobcontrol import JobController

# main
if __name__ == '__main__':

    con = JobController() 
    con.set_user('menjo')
    con.make_directories()
    con.setup_logger(logfile="./submitlog/log.txt")

    # DoubleArm Mass Production for >3 hit study 
    con.loop_jobs(model='EPOSLHC', srun=10001, erun=20000, limit=500, orgfile='run_crmc_darm.sh')

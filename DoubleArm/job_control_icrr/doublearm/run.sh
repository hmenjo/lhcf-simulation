#!/bin/bash
#------ pjsub option --------#
#PJM -L "rscgrp=A"
#PJM -L "vnode=1"
#PJM -L "vnode-core=1"
#PJM -j
#PJM -o "/disk/lhcf/user/simulation_Run3/Data/DoubleArm/v1/EPOSLHC/log/log_run$1.out"
#------- Program execution -------#
# History
#    2022 Nov. 11th : copied from the original file in /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/run/run.sh and removed script related to conversion and transports. 

source /disk/lhcf/user/simulation_Run3/lhcf-simulation/DoubleArm/setup_epics_icrr.sh

RUN=$1
RUNTMP=`printf %06d $RUN`

MODEL="EPOSLHC"

DATADIR="/disk/lhcf/user/simulation_Run3/Data/"
WORKDIR="${DATADIR}/DoubleArm/tmp/${MODEL}_${RUN}/"
INPUT_DIR="${DATADIR}/crmc_1.8.0/hepmc2gencol/${MODEL}/"
INPUT_FILENAME="gen_${MODEL}_${RUNTMP}.out"
OUTPUT_DIR="${DATADIR}/DoubleArm/v1/${MODEL}/"
OUTPUT_FILENAME="darm_${MODEL}_${RUNTMP}.out"

# Setup 
mkdir -p ${WORKDIR}
mkdir -p ${OUTPUT_DIR}/log
mkdir -p ${OUTPUT_DIR}/old
cd ${WORKDIR}

date

# Check  the input file
if [ -f "$INPUT_DIR/$INPUT_FILENAME.gz" ];then
    gunzip -c $INPUT_DIR/$INPUT_FILENAME.gz > ./+primary
elif [ -f "$INPUT_DIR/$INPUT_FILENAME"];then
    cp $INPUT_DIR/$INPUT_FILENAME ./+primary
else 
    echo "ERROR: No input file was found"
    exit
fi 
# Check presence of old file
if [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME.gz" ];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/
    echo "mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/"
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
elif [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME"];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME $OUTOUT_DIR/old
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
fi

DOUBLEARM_DIR="${LHCF_SIM_BASE}/DoubleArm/src/" 
DOUBLEARM_BIN="sepicsLHCPCLinuxIFC64"
# copy files to the working directory
cp $DOUBLEARM_DIR/$DOUBLEARM_BIN ./
cp $DOUBLEARM_DIR/FirstInput* ./
cp $DOUBLEARM_DIR/param ./
cp $DOUBLEARM_DIR/dpmjet.* ./
cp $DOUBLEARM_DIR/configdarm* ./
cp $DOUBLEARM_DIR/sepicsfile_jobs ./
cp $DOUBLEARM_DIR/epicsfile ./

# replace the random seed 
sed -e "s/-123456/$RUN/g" sepicsfile_jobs > sepicsfile

./$DOUBLEARM_BIN < FirstInput_nom > $OUTPUT_DIR/$OUTPUT_FILENAME 

# gzip the output file
gzip $OUTPUT_DIR/$OUTPUT_FILENAME

# remove the directory 
rm -rf $WORKDIR

date

source /opt/exp_software/lhcf/berti/intel_ifort/composer_xe_2013_sp1.3.174/bin/compilervars.sh intel64
INTEL_LICENSE_FILE=/opt/exp_software/lhc/berti/intel/licenses/l_3CPW65S3.lic

#export COSMOSTOP=/opt/exp_software/lhcf/Simulation/Cosmos8.042/
export COSMOSTOP=/opt/exp_software/lhcf/Software/Cosmos8.042/
export COSMOSINC=${COSMOSTOP}/cosmos/
export PATH=$COSMOSTOP/Scrpt/:$PATH

#export EPICSTOP=/opt/exp_software/lhcf/Simulation/Epics9.311/
export EPICSTOP=/opt/exp_software/lhcf/Software/Epics9.311/
export EPICSINC=$EPICSTOP/epics
export PATH=$EPICSTOP/Scrpt/:$PATH
